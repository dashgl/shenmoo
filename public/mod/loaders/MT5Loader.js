import {
	FileLoader,
	Loader,
	LoaderUtils,
} from "../../three.module.js";

import * as THREE from '../../three.module.js';

var MT5Loader = ( function () {

	/**
	 * ANIM BUILDER
	 **/

	function AnimBuilder(mesh) {
		
		this.bones = mesh.skeleton.bones;
		this.mem = {};
		this.nodes = [];
		this.rot_only = mesh.userData.rot_only;
		
		console.log("Only rotation!!");
		console.log(this.rot_only);

	}

	AnimBuilder.prototype = {

		constructor: MeshBuilder,

		parse : function ( data, resourcePath, onProgress, onError ) {

			this.view = new DataView(data);
			this.readHeader();
			this.readBlock1();

			for(let i = 0; i < this.nodes.length; i++) {
				let tmp = {};

				switch(this.nodes[i].node_id) {
				case 29:
				case 35:
					tmp.x = this.nodes[i].x;
					tmp.y = this.nodes[i].y;
					tmp.z = this.nodes[i].z;

					this.nodes[i].x = tmp.z;
					this.nodes[i].y = tmp.y;
					this.nodes[i].z = tmp.x;
					break;
				}
			}

			this.fillInValues();

			for(let i = 0; i < this.nodes.length; i++) {
				
				let node = this.nodes[i];
				switch(node.node_id) {
				case 29:
				case 35:
					console.log(node);
					for(let k = 0; k < node.z.length; k++) {
						node.z[k] += this.nodes[0].z[k];
					}
					break;
				}
			}

			return this.createAnimation();

		},

		createAnimation : function() {

			const anim_name = "walk";
			let duration = this.mem.anim_length / 30;
			const tracks = [];

			this.nodes.forEach( node => {
				
				if(!node.bone) {
					return;
				}

				if(node.type === 'pos') {
					
					const bone = node.bone;
					const name = ".bones[" + bone.name + "].position";
					const times = [];
					const values = [];
				
					for(let i = 0; i < this.mem.anim_length; i++) {
					
						times.push(i / 30);
						values.push(node.x[i], node.y[i], node.z[i]);

					}

					const track = new THREE.VectorKeyframeTrack(name, times, values);
					tracks.push(track);

				} else {
					
					const bone = node.bone;
					let localRot = node.localRot.matrix;
					let worldRot = node.localRot.matrixWorld;
					let pLocalRot = node.localRot.parent.matrix;
					let pWorldRot = node.localRot.parent.matrixWorld;
					pWorldRot = pWorldRot.getInverse(pWorldRot);
					pWorldRot.transpose();

					const name = ".bones[" + bone.name + "].quaternion";
					const times = [];
					const values = [];

					for(let i = 0; i < this.mem.anim_length; i++) {
					
						times.push(i / 30);					
						const localMat = new THREE.Matrix4();

						const rotX = new THREE.Matrix4();
						rotX.makeRotationX(node.x[i]);
						localMat.multiply(rotX);

						const rotY = new THREE.Matrix4();
						rotY.makeRotationY(node.y[i]);
						localMat.multiply(rotY);

						const rotZ = new THREE.Matrix4();
						rotZ.makeRotationZ(node.z[i]);
						localMat.multiply(rotZ);

						localMat.multiply(pWorldRot);
						const quat = new THREE.Quaternion();
						quat.setFromRotationMatrix(localMat);
						
						values.push(quat.x, quat.y, quat.z, quat.w);

					}
					
					const track = new THREE.QuaternionKeyframeTrack(name, times, values);
					tracks.push(track);
					

				}

			});
			
			let clip = new THREE.AnimationClip(anim_name, duration, tracks);
			clip.optimize();
			return clip;

		},

		fillInValues : function() {

			const axis_list = [ 'x', 'y', 'z' ];
			for(let n = 0; n < this.nodes.length; n++) {
				
				let node = this.nodes[n];
				// If we don't have a bone we can ignore

				if(!node.bone) {
					continue;
				}

				axis_list.forEach( axis => {
					
					const array = new Array(this.mem.anim_length);

					let start = node[axis].shift();
					let end = node[axis].pop();
					const source = [ start ];

					for(let i = 0; i < node[axis].length; i++) {
						let keys = Object.keys(node[axis][i]);
						if(keys.indexOf('val') === -1) {
							continue;
						}
						source.push(node[axis][i]);
					}

					source.push(end);

					for(let i = 0; i < source.length; i++) {
						const a = source[i];
						const b = source[i + 1];
						
						if(!a.val) {
							a.val = node.bone.position[axis];
						}

						if(!b) {
							array[a.frame] = a.val;
							break;
						}
						
						if(!b.val) {
							b.val = node.bone.position[axis];
						}

						let diff = (b.val - a.val) / (b.frame - a.frame);
						for(let k = a.frame; k < b.frame; k++) {
							array[k] = a.val + (diff * (k - a.frame))
						}

					}

					node[axis] = array;

				});

			}
			
			console.log("Filled in values!!!");
			console.log(this.nodes);

		},

		readHeader : function() {

			this.mem.anim_length = this.view.getUint16(0x00, true) - 1;
			this.mem.block1Ofs = 0x0c;
			this.mem.block2Ofs = this.view.getUint16(0x04, true);
			this.mem.block3Ofs = this.view.getUint16(0x06, true);
			this.mem.block4Ofs = this.view.getUint16(0x08, true);
			this.mem.block5Ofs = this.view.getUint16(0x0a, true);

			this.mem.block_2_half = true;
			this.mem.block_3_half = true;

		},

		readBlock1() {

			const pos_mask = 0x1c0;
			const rot_mask = 0x38;
			
			let instruction;

			console.log(this.bones);

			do {

				instruction = this.view.getUint16(this.mem.block1Ofs, true);
				this.mem.block1Ofs += 2;

				if(instruction === 0) {
					break;
				}
				
				const node_id = instruction >> 9;
				let bone, localRot;

				switch(node_id) {
				case 0:
					// Center
					// bone = this.bones[0];
					localRot = this.rot_only[0];
					break;
				case 5:
					// Right Thigh
					bone = this.bones[30];
					localRot = this.rot_only[30];
					console.log("BOOOM!!!");
					console.log(localRot);

					break;
				case 8:
					// Ik Target - Right Foot
					bone = this.bones[38];
					localRot = this.rot_only[38];
					break;
				case 15:
					// Ik Target - Left Foot
					bone = this.bones[39];
					localRot = this.rot_only[39];
					break;
				case 29:
					// Ik Target - Right Hand
					bone = this.bones[40];
					localRot = this.rot_only[40];
					break;
				case 35:
					// Ik Target - Left Hand
					bone = this.bones[41];
					localRot = this.rot_only[41];
					break;
				}

				// Read Position

				if(instruction & pos_mask) {
					
					const node = {
						node_id : node_id,
						type : 'pos',
						bone : bone,
						localRot : localRot,
						x : [],
						y : [],
						z : []
					}
					
					this.mem.type = 'pos';

					if(instruction & 0x100) {
						this.readBlock2(node.x);
					}

					if(instruction & 0x80) {
						this.readBlock2(node.y);
					}

					if(instruction & 0x40) {
						this.readBlock2(node.z);
					}
					
					this.nodes.push(node);

				}

				// Read Rotations

				if(instruction & rot_mask) {

					const node = {
						node_id : node_id,
						type : 'rot',
						bone : bone,
						localRot : localRot,
						x : [],
						y : [],
						z : []
					}

					this.mem.type = 'rot';

					if(instruction & 0x20) {
						this.readBlock2(node.x);
					}

					if(instruction & 0x10) {
						this.readBlock2(node.y);
					}

					if(instruction & 0x08) {
						this.readBlock2(node.z);
					}

					this.nodes.push(node);

				}

			} while(instruction !== 0);
			
		},

		readBlock2 : function(array) {
			
			let keyFrameCount;

			if(this.mem.block_2_half) {
				keyFrameCount = this.view.getUint8(this.mem.block2Ofs);
				this.mem.block2Ofs++;
			} else {
				keyFrameCount = this.view.getUint16(this.mem.block2Ofs, true);
				this.mem.block2Ofs += 2;
			}
			
			this.readBlock3(keyFrameCount, array);

		},

		readBlock3 : function(keyFrameCount, array) {

			array.push({
				frame : 0
			});
			
			for(let i = 0; i < keyFrameCount; i++) {
				let keyFrame;

				if(this.mem.block_3_half) {
					keyFrame = this.view.getUint8(this.mem.block3Ofs);
					this.mem.block3Ofs++;
				} else {
					keyFrame = this.view.getUint16(this.mem.block3Ofs, true);
					this.mem.block3Ofs += 2;
				}

				array.push({
					frame : keyFrame
				})
			}
			
			array.push({
				frame : this.mem.anim_length
			});

			this.readBlock4(array);
		},

		readBlock4 : function(array) {
			
			let bits = [
				{ m : 0xc0, a : 0x80, b : 0x40 }, 
				{ m : 0x30, a : 0x20, b : 0x10 }, 
				{ m : 0x0c, a : 0x08, b : 0x04 }, 
				{ m : 0x03, a : 0x02, b : 0x01 }, 
			];

			let index = 0;
			do {

				let byte = this.view.getUint8(this.mem.block4Ofs);
				this.mem.block4Ofs++

				if(byte === 0) {
					break;
				}

				bits.forEach( set => {

					if((byte & set.m) === 0) {
						return;
					}
					
					if(byte & set.a) {
						let slope_l = this.view.getUint16(this.mem.block5Ofs, true);
						this.mem.block5Ofs += 2;
						let slope_r = this.view.getUint16(this.mem.block5Ofs, true);
						this.mem.block5Ofs += 2;

						array[index].curve = [
							this.decodeFloat16(slope_l),
							this.decodeFloat16(slope_r)
						]
					};
					
					if(byte & set.b) {
						let val = this.view.getUint16(this.mem.block5Ofs, true);
						this.mem.block5Ofs += 2;
						array[index].val = this.decodeFloat16(val);
						if (this.mem.type === 'rot') {
							array[index].val *= 65535;
						}
					}

					index++;

				});
				
			} while(index < array.length);

		},

		decodeFloat16 : function (binary) {

			let exponent = (binary & 0x7C00) >> 10;
			let fraction = binary & 0x03FF;

			return (binary >> 15 ? -1 : 1) * (
				exponent ?
				(
					exponent === 0x1F ?
					fraction ? NaN : Infinity :
					Math.pow(2, exponent - 15) * (1 + fraction / 0x400)
				) : 6.103515625e-5 * (fraction / 0x400)
			);

		}

	}

	/**
	 * MESH BUILDER
	 **/

	function MeshBuilder( mats ) {
		
		this.bones = [];
		this.vertices = [];
		this.vertex_lookup = [];
		this.vertex_weight = [];
		this.vertex_indices = [];
		this.faces = [];
		this.faceVertexUvs = [];
		this.polygonCount = 0;
		this.vertexOfs = 0;
		this.iks = [];
		this.materials = mats;
		this.rot_only = [];

	}

	MeshBuilder.prototype = {

		constructor: MeshBuilder,

		parse : function ( data, resourcePath, onProgress, onError ) {
	
			this.view = new DataView(data);

			this.header = {
				magic : this.view.getUint32(0x00, true),
				textureOfs : this.view.getUint32(0x04, true),
				modelOfs : this.view.getUint32(0x08, true)
			}

			var geometry = this.buildGeometry();
			
			var material = new THREE.MeshNormalMaterial({
				skinning : true
			});
			
			this.addTargets();
			
			var mesh = new THREE.SkinnedMesh( geometry, this.materials );
			var skeleton = new THREE.Skeleton( this.bones );
			mesh.add(skeleton.bones[0]);
			mesh.bind( skeleton );
			
			for(let i = 1; i < this.bones.length; i++) {
				if(this.bones[i].parent) {
					continue;
				}
				mesh.add(this.bones[i]);
			}
			
			mesh.userData.rot_only = this.rot_only;
			mesh.geometry.userData.MMD = {
				bones: this.bones,
				iks: this.iks,
				grants: [],
				rigidBodies: [],
				constraints: [],
				format: "pmd"
			}
			
			return mesh;
		},

		buildGeometry : function() {
			
			this.firstStrip = true;
			this.ofs = this.header.modelOfs;
			this.readBone();
			
			let geometry = new THREE.Geometry();
			this.vertices.forEach( v => {
				geometry.vertices.push(v);
			});
		
			this.faces.forEach( f => {
				geometry.faces.push(f);
			});
			
			this.vertex_weight.forEach( w => {
				let weight = new THREE.Vector4(w, 0, 0, 0);
				geometry.skinWeights.push(weight);
			});
			
			this.vertex_indices.forEach( i => {
				let index = new THREE.Vector4(i, 0, 0, 0);
				geometry.skinIndices.push(index);
			});
			
			this.faceVertexUvs.forEach(face => {
				geometry.faceVertexUvs[0].push(face);
			});

			geometry.computeFaceNormals();
			let buffer = new THREE.BufferGeometry();
			buffer.fromGeometry(geometry);
			buffer.computeBoundingSphere();

			return buffer;
		},

		addTargets : function() {

			// Right Foot Target

			let rightFootTarget = new THREE.Bone();
			rightFootTarget.name = "leg_IK_R";
			this.bones.push(rightFootTarget);
			
			/*
			this.iks.push({
				effector : 32,
				iteration : 40,
				links : [ {
					index : 31,
					enabled : true,
					limitation : new THREE.Vector3(-1,0,0)
				}, {
					index : 30, 
					enabled : true, 
				}],
				maxAngle : Math.PI,
				target : 38
			});
			*/

			// Left Foot Target
			
			let leftFootTarget = new THREE.Bone();
			leftFootTarget.name = "leg_IK_L";
			this.bones.push(leftFootTarget);
			
			/*
			this.iks.push({
				effector : 36,
				iteration : 40,
				links : [
				{
					index : 35,
					enabled : true,
					limitation : new THREE.Vector3(-1,0,0)
				},
				{
					index : 34, 
					enabled : true, 
				}],
				maxAngle : Math.PI,
				target : 39
			});
			*/

			// Right Hand Target

			let rightHandTarget = new THREE.Bone();
			rightHandTarget.name = "hand_IK_R";
			this.bones.push(rightHandTarget);

			let v3 = new THREE.Vector3();
			v3.applyMatrix4(this.bones[8].matrixWorld);
			rightHandTarget.position.x = v3.x;
			rightHandTarget.position.y = v3.y;
			rightHandTarget.position.z = v3.z;

			rightHandTarget.updateMatrix();
			rightHandTarget.updateMatrixWorld();
			
			/*
			this.iks.push({
				effector : 8,
				iteration : 40,
				links : [
				{
					index : 7,
					enabled : true,
					limitation : new THREE.Vector3(-1,0,0)
				},
				{
					index : 6, 
					enabled : true, 
				}],
				maxAngle : Math.PI,
				target : 40
			});
			*/

			// Left Hand Target

			let leftHandTarget = new THREE.Bone();
			leftHandTarget.name = "hand_IK_L";
			this.bones.push(leftHandTarget);

			let v4 = new THREE.Vector3();
			v4.applyMatrix4(this.bones[18].matrixWorld);
			leftHandTarget.position.x = v4.x;
			leftHandTarget.position.y = v4.y;
			leftHandTarget.position.z = v4.z;
			
			/*
			this.iks.push({
				effector : 18,
				iteration : 40,
				links : [
				{
					index : 17,
					enabled : true,
					limitation : new THREE.Vector3(-1,0,0)
				},
				{
					index : 16, 
					enabled : true, 
				}],
				maxAngle : Math.PI,
				target : 41
			});
			*/

		},

		readBone : function(parentBone) {

			const c = (2 * Math.PI / 0xFFFF);

			const node = {
				flag : this.view.getUint32(this.ofs + 0x00, true),
				polygonOfs : this.view.getUint32(this.ofs + 0x04, true),
				rot : {
					x : this.view.getInt32(this.ofs + 0x08, true) * c,
					y : this.view.getInt32(this.ofs + 0x0c, true) * c,
					z : this.view.getInt32(this.ofs + 0x10, true) * c
				},
				scl : {
					x : this.view.getFloat32(this.ofs + 0x14, true),
					y : this.view.getFloat32(this.ofs + 0x18, true),
					z : this.view.getFloat32(this.ofs + 0x1c, true)
				},
				pos : {
					x : this.view.getFloat32(this.ofs + 0x20, true),
					y : this.view.getFloat32(this.ofs + 0x24, true),
					z : this.view.getFloat32(this.ofs + 0x28, true)
				},
				childOfs : this.view.getUint32(this.ofs + 0x2c, true),
				siblingOfs : this.view.getUint32(this.ofs + 0x30, true),
				parentBoneId : this.view.getUint32(this.ofs + 0x34, true),
				unknown_a : this.view.getUint32(this.ofs + 0x38, true),
				unknown_b : this.view.getUint32(this.ofs + 0x3c, true)
			};

			if(!parentBone) {
				node.pos.y = 1.1;
			}

			this.bone = new THREE.Bone();
			const bone = new THREE.Bone();
			const rotBone = new THREE.Bone();

			let num = this.bones.length;
			let str = num.toString();
			while(str.length < 3) {
				str = '0' + str;
			}
			this.bone.name = 'bone_' + str;
			bone.name = 'bone_' + str;

			this.bone.userData = this.bone.userData || {};
			this.bone.userData.id = num;
			this.bone.userData.bone = bone;
			this.bone.userData.rot = rotBone;

			// Scale

			this.bone.scale.x = node.scl.x;
			this.bone.scale.y = node.scl.y;
			this.bone.scale.z = node.scl.z;

			// Rotation
			
			for(let axis in node.rot) {
				let mat = new THREE.Matrix4();
				switch(axis) {
				case 'x':
					mat.makeRotationX(node.rot[axis]);
					break;
				case 'y':
					mat.makeRotationY(node.rot[axis]);
					break;
				case 'z':
					mat.makeRotationZ(node.rot[axis]);
					break;
				}
				this.bone.applyMatrix4(mat);
				rotBone.applyMatrix4(mat);
			}
			// Position

			this.bone.position.x = node.pos.x;
			this.bone.position.y = node.pos.y;
			this.bone.position.z = node.pos.z;

			rotBone.updateMatrix();
			rotBone.updateMatrixWorld();

			this.bone.updateMatrix();
			this.bone.updateMatrixWorld();

			// Parent Bone
			
			if(!parentBone) {
			
				bone.position.x = this.bone.position.x;
				bone.position.y = this.bone.position.y;
				bone.position.z = this.bone.position.z;
				bone.updateMatrix();
				bone.updateMatrixWorld();

			} else {
				parentBone.add(this.bone);
				let p = this.bones[parentBone.userData.id];
				p.add(bone);
				p.userData.rot.add(rotBone);

				this.bone.updateMatrix();
				this.bone.updateMatrixWorld();

				const pb = new THREE.Vector3();
				const cb = new THREE.Vector3();

				pb.applyMatrix4(parentBone.matrixWorld)
				cb.applyMatrix4(this.bone.matrixWorld)
		
				bone.position.x = cb.x - pb.x;
				bone.position.y = cb.y - pb.y;
				bone.position.z = cb.z - pb.z;

				bone.updateMatrix();
				bone.updateMatrixWorld();

				rotBone.updateMatrix();
				rotBone.updateMatrixWorld();
			}

			bone.userData = bone.userData || {}
			bone.userData.rot = rotBone;
			this.bones.push(bone);
			this.rot_only.push(rotBone);
			
			switch(num) {
			case 0:
				this.bone.name = "chest";
				break;
			case 1:
				this.bone.name = "head";
				break;
			case 2:
				this.bone.name = "hair";
				break;
			case 3:
				this.bone.name = "shoulder_r";
				break;
			case 4:
				this.bone.name = "elbow_r";
				break;
			case 5:
				this.bone.name = "wrist_r";
				break;
			case 29:
				this.bone.name = "hips";
				break;
			case 30:
				this.bone.name = "thigh_r";
				break;
			case 31:
				this.bone.name = "knee_r";
				break;
			case 32:
				this.bone.name = "ankle_r";
				break;
			case 33:
				this.bone.name = "foot_r";
				break;
			case 34:
				this.bone.name = "thigh_l";
				break;
			case 35:
				this.bone.name = "knee_l";
				break;
			case 36:
				this.bone.name = "ankle_l";
				break;
			case 37:
				this.bone.name = "foot_l";
				break;
			}

			bone.name = this.bone.name;

			// Read Attached Mesh

			if(node.polygonOfs) {
				this.ofs = node.polygonOfs;
				this.readPolygon();
			}

			// Read Child and Sibling Nodes

			if(node.childOfs) {
				this.ofs = node.childOfs;
				this.readBone(this.bone);
			}

			if(node.siblingOfs) {
				this.ofs = node.siblingOfs;
				this.readBone(parentBone);
			}

		},

		readPolygon : function() {

			// Read Polygon Struct
			
			const polygon = {
				flag : this.view.getUint32(this.ofs + 0x00, true),
				vertexOfs  : this.view.getUint32(this.ofs + 0x04, true),
				vertexCount : this.view.getUint32(this.ofs + 0x08, true),
				stripOfs : this.view.getUint32(this.ofs + 0x0c, true),
				center : {
					x : this.view.getFloat32(this.ofs + 0x10, true),
					y : this.view.getFloat32(this.ofs + 0x14, true),
					z : this.view.getFloat32(this.ofs + 0x18, true)
				},
				radius : this.view.getFloat32(this.ofs + 0x1c, true)
			};

			// Read Weighted Vertices
			
			this.ofs = polygon.vertexOfs;
			for(let i = 0; i < polygon.vertexCount; i++) {
				
				const pos = {
					x : this.view.getFloat32(this.ofs + 0x00, true), 
					y : this.view.getFloat32(this.ofs + 0x04, true), 
					z : this.view.getFloat32(this.ofs + 0x08, true) 
				}
				
				const norm = {
					x : this.view.getFloat32(this.ofs + 0x0c, true), 
					y : this.view.getFloat32(this.ofs + 0x10, true), 
					z : this.view.getFloat32(this.ofs + 0x14, true) 
				}

				this.ofs += 0x18;
				const vertex = new THREE.Vector3();
				vertex.x = pos.x;
				vertex.y = pos.y;
				vertex.z = pos.z;
				vertex.applyMatrix4(this.bone.matrixWorld);
				this.vertex_lookup[i] = this.vertices.length;
				this.vertex_weight.push(1.0);
				this.vertex_indices.push(this.bone.userData.id);
				this.vertices.push(vertex);

			}
			
			if(polygon.stripOfs) {
				this.ofs = polygon.stripOfs;
				this.readStrips(polygon);
			}

			this.vertexOfs += polygon.vertexCount;
			this.bone.userData.vertexOfs = this.vertexOfs;

		},

		readStrips : function(polygon) {

			// Read Strips
			
			let vertexOfs = 0;
			if(this.bone.parent) {
				vertexOfs = this.bone.parent.userData.vertexOfs;
			}

			this.ofs = polygon.stripOfs;
			
			let texId;
			do {

				let stripType = this.view.getUint16(this.ofs, true);
				this.ofs += 2;

				if(stripType === 0x0000 || stripType === 0xffff) {
					continue;
				} else if(stripType === 0x0e) {
					
					// Material Definition

					let byteLen = this.view.getUint16(this.ofs, true);
					this.ofs += 2;

					// Diffuse ? Ambient ?
					this.ofs += byteLen;

				} else if(stripType === 0x02) {

					let byteLen = this.view.getUint16(this.ofs, true);
					this.ofs += 2;

					// Unknown

					this.ofs += byteLen;
					

				} else if(stripType === 0x8000) {
					
					break;

				} else if(stripType === 0x11) {
					
					let byteLen = this.view.getInt16(this.ofs, true);
					this.ofs += 2;
					
					let stripCount = this.view.getInt16(this.ofs, true);
					this.ofs += 2;

					for(let i = 0; i < stripCount; i++) {

						let strip = [];
						let stripLen = this.view.getInt16(this.ofs, true);
						this.ofs += 2;

						for(let i = 0; i < Math.abs(stripLen); i++) {
							let idx = this.view.getInt16(this.ofs, true);
							this.ofs += 2;

							let index;
							if(idx >= 0) {
								index = idx + this.vertexOfs;
							} else {
								index = idx + vertexOfs;
							}
							
							let u = this.view.getInt16(this.ofs, true);
							this.ofs += 2;

							let v = this.view.getInt16(this.ofs, true);
							this.ofs += 2;

							strip.push ({
								i : index,
								u : u / 0x3ff,
								v : v / 0x3ff,
							});
						}

						
						let clockwise = stripLen < 0;
						for(let i = 0; i < strip.length - 2; i++) {
							let a, b, c;

							if(clockwise && i % 2 === 0) {
								a = strip[i + 1];
								b = strip[i + 0];
								c = strip[i + 2];
							} else {
								a = strip[i + 0];
								b = strip[i + 1];
								c = strip[i + 2];
							}

							let face = new THREE.Face3(a.i, b.i, c.i);
							face.materialIndex = texId;
							this.faces.push(face);

							let auv = new THREE.Vector2(a.u, a.v);
							let buv = new THREE.Vector2(b.u, b.v);
							let cuv = new THREE.Vector2(c.u, c.v);
							this.faceVertexUvs.push([ auv, buv, cuv ]);

						} 
						
					}

				} else if(stripType === 0x09) {

					texId = this.view.getUint16(this.ofs, true);
					this.ofs += 2;

				} else if(stripType === 0x0b) {
					
					let uvRatio = this.view.getUint16(this.ofs, true);
					this.ofs+= 2;

				} else if(stripType === 0x08) {
					
					let flag = this.view.getUint16(this.ofs, true);

				} if(stripType === 0x03) {
					
					let byteLen = this.view.getUint16(this.ofs, true);
					this.ofs += 2;
					this.ofs += byteLen;

				} 

			} while(this.ofs < polygon.vertexOfs);

			this.polygonCount++;

		}

	}

	/**
	 * @param {THREE.LoadingManager} manager
	 */

	function MT5Loader( manager ) {

		Loader.call( this, manager );

		this.loader = new FileLoader( this.manager );

		this.parser = null; // lazy generation

	}

	MT5Loader.prototype = Object.assign( Object.create( Loader.prototype ), {

		constructor : MT5Loader,

		load: function ( url, onLoad, onProgress, onError ) {

			// resource path

			var resourcePath;

			if ( this.resourcePath !== '' ) {

				resourcePath = this.resourcePath;

			} else if ( this.path !== '' ) {

				resourcePath = this.path;

			} else {

				resourcePath = LoaderUtils.extractUrlBase( url );

			}

			let materials = [];
			for(let i = 0; i < 10; i++) {
				let path = 'assets/ryo_0' + i + '.png';
				let texture = new THREE.TextureLoader().load(path);
				let material = new THREE.MeshBasicMaterial( { 
					map: texture,
					skinning : true
				} );
				materials.push(material);
			}

			this.loader
				.setMimeType( undefined )
				.setPath( this.path )
				.setResponseType( 'arraybuffer' )
				.setRequestHeader( this.requestHeader )
				.load( url, function ( buffer ) {

					let builder = new MeshBuilder(materials);
					let mesh = builder.parse(buffer, resourcePath, onProgress, onError);
					onLoad( mesh );

				}, onProgress, onError );

		},

		loadAnimation: function ( url, mesh, onLoad, onProgress, onError ) {

			// resource path

			var resourcePath;

			if ( this.resourcePath !== '' ) {

				resourcePath = this.resourcePath;

			} else if ( this.path !== '' ) {

				resourcePath = this.path;

			} else {

				resourcePath = LoaderUtils.extractUrlBase( url );

			}

			this.loader
				.setMimeType( undefined )
				.setPath( this.path )
				.setResponseType( 'arraybuffer' )
				.setRequestHeader( this.requestHeader )
				.load( url, function ( buffer ) {

					let builder = new AnimBuilder(mesh);
					let anim = builder.parse(buffer, resourcePath, onProgress, onError);
					onLoad( anim );

				}, onProgress, onError );

		},

		loadWithAnimation : function( modelUrl, motnUrl, onLoad, onProgress, onError ) {
			
			var scope = this;

			this.load( modelUrl, function ( mesh ) {

				 scope.loadAnimation( motnUrl, mesh, function ( anim ) {

				 	onLoad( mesh, anim );

				 }, onProgress, onError);


			}, onProgress, onError );

		}

	});

	return MT5Loader;


} )();

export { MT5Loader };
