"use strict";

THREE.MT5Loader = class {

	constructor(buffer) {
		
		// Attributes for reading binary

		this.ofs = 0;
		this.view = new DataView(buffer);
		
		// Attributes for reading geometry 

		this.bone = null;
		this.bones = [];
		this.position = [];

		this.ATTR = {
			pos : [],
			norm : [],
			diffuse_uv : []
		}

	}

	parse() {

		// Struct 1 
		// Start of the file contains 
		// Magic Number
		// Pointer to geometry
		// Pointer to textures

		const header = {
			magic : this.view.getUint32(0x00, true),
			textureOfs : this.view.getUint32(0x04, true),
			modelOfs : this.view.getUint32(0x08, true)
		};
		
		this.ofs = header.modelOfs;
		this.readBone();
		let iks = this.metaBones();

		var geometry = new THREE.CylinderBufferGeometry( 5, 5, 5, 5, 15, 5, 30 );
		geometry.userData.MMD = {
			bones : this.bones,
			iks : iks
		}

		let material = new THREE.MeshBasicMaterial({ visible : false });
		var mesh = new THREE.SkinnedMesh( geometry, material );

		let skeleton = new THREE.Skeleton( this.bones );
		var rootBone = skeleton.bones[ 0 ];
		mesh.add( rootBone );
		mesh.bind( skeleton );
		
		console.log("Bone Length: %d", skeleton.bones.length);

		
		return mesh;

	}

	metaBones() {
		
		const iks = new Array();
		let num, str;

		// Right Foot IK Target

		const rf_target = new THREE.Bone();
		num = this.bones.length;
		str = num.toString();
		while(str.length < 3) {
			str = "0" + str;
		}
		rf_target.name = "meta_" + str;

		iks.push({
			target : num,
			effector : 33,
			links : [
				{
					index : 32, 
					limitation : null,
					rotationMin : null,
					rotationMax : null,
					enabled : true
				},
				{
					index : 31, 
					limitation : null,
					rotationMin : null,
					rotationMax : null,
					enabled : true
				},
				{
					index : 30, 
					limitation : null,
					rotationMin : null,
					rotationMax : null,
					enabled : true
				}
			],
			iteration : 40,
			minAngle : null,
			maxAngle : null
		});

		this.bones.push(rf_target);
		return iks;

	}

	readBone(parentBone) {

		// Struct 2
		// Bone structure containts
		// Flag (for transformations)
		// Pointer to bone weighted polygon (vertices + strip)
		// Rotation
		// Scale
		// Position
		// Pointer to child bone
		// Pointer to sibling bone
		// Parent Bone Id

		const c = (2 * Math.PI / 0xFFFF);

		const node = {
			flag : this.view.getUint32(this.ofs + 0x00, true),
			polygonOfs : this.view.getUint32(this.ofs + 0x04, true),
			rot : {
				x : this.view.getInt32(this.ofs + 0x08, true) * c,
				y : this.view.getInt32(this.ofs + 0x0c, true) * c,
				z : this.view.getInt32(this.ofs + 0x10, true) * c
			},
			scl : {
				x : this.view.getFloat32(this.ofs + 0x14, true),
				y : this.view.getFloat32(this.ofs + 0x18, true),
				z : this.view.getFloat32(this.ofs + 0x1c, true)
			},
			pos : {
				x : this.view.getFloat32(this.ofs + 0x20, true),
				y : this.view.getFloat32(this.ofs + 0x24, true),
				z : this.view.getFloat32(this.ofs + 0x28, true)
			},
			childOfs : this.view.getUint32(this.ofs + 0x2c, true),
			siblingOfs : this.view.getUint32(this.ofs + 0x30, true),
			parentBoneId : this.view.getUint32(this.ofs + 0x34, true),
			unknown_a : this.view.getUint32(this.ofs + 0x38, true),
			unknown_b : this.view.getUint32(this.ofs + 0x3c, true)
		}

		this.bone = new THREE.Bone();
		let num = this.bones.length;

		let str = num.toString();
		while(str.length < 3) {
			str = "0" + str;
		}

		this.bone.name = "bone_" + str;
		this.bone.userData = this.bone.userData || {};
		this.bone.userData.id = num;

		this.bone.scale.x = node.scl.x;
		this.bone.scale.y = node.scl.y;
		this.bone.scale.z = node.scl.z;

		let lMatrix = new THREE.Matrix4();
		lMatrix.makeRotationX(node.rot.x);
		this.bone.applyMatrix4(lMatrix);

		lMatrix = new THREE.Matrix4();
		lMatrix.makeRotationY(node.rot.y);
		this.bone.applyMatrix4(lMatrix);

		lMatrix = new THREE.Matrix4();
		lMatrix.makeRotationZ(node.rot.z);
		this.bone.applyMatrix4(lMatrix);

		this.bone.position.x = node.pos.x;
		this.bone.position.y = node.pos.y;
		this.bone.position.z = node.pos.z;

		this.bone.updateMatrix();
		this.bone.updateMatrixWorld();
		
		if(num === 29) {
			console.log("ROattions!!!");
			console.log(node.rot);
		}

		if(parentBone) {
			parentBone.add(this.bone);
			this.bone.updateMatrix();
			this.bone.updateMatrixWorld();
		}

		this.bones.push(this.bone);

		if(node.polygonOfs) {
			this.ofs = node.polygonOfs;
			// this.readPolygon();
		}

		if(node.childOfs) {
			this.ofs = node.childOfs;
			this.readBone(this.bone);
		}

		if(node.siblingOfs) {
			this.ofs = node.siblingOfs;
			this.readBone(parentBone);
		}

	}

	readPolygon() {

		// Struct 3
		// Polygon is a sub-mesh of weighted vertices and strips
		// to the current defined bone


		const polygon = {
			flag : this.view.getUint32(this.ofs + 0x00, true),
			vertexOfs  : this.view.getUint32(this.ofs + 0x04, true),
			vertexCount : this.view.getUint32(this.ofs + 0x08, true),
			stripOfs : this.view.getUint32(this.ofs + 0x0c, true),
			center : {
				x : this.view.getFloat32(this.ofs + 0x10, true),
				y : this.view.getFloat32(this.ofs + 0x14, true),
				z : this.view.getFloat32(this.ofs + 0x18, true)
			},
			radius : this.view.getFloat32(this.ofs + 0x1c, true)
		};

		console.log("Vertex offset: 0x%s", polygon.vertexOfs.toString(16));
		console.log("Vertex Count: %d", polygon.vertexCount);
		console.log("Strip Offset: 0x%s", polygon.stripOfs.toString(16));

		// First read the vertex list

		this.ofs = polygon.vertexOfs;
		let vertices = [];

		for(let i = 0; i < polygon.vertexCount; i++) {
			
			vertices[i] = {
				pos : {
					x : this.view.getFloat32(this.ofs + 0x00, true), 
					y : this.view.getFloat32(this.ofs + 0x04, true), 
					z : this.view.getFloat32(this.ofs + 0x08, true)
				},
				norm : {
					x : this.view.getFloat32(this.ofs + 0x0c, true), 
					y : this.view.getFloat32(this.ofs + 0x10, true), 
					z : this.view.getFloat32(this.ofs + 0x14, true)
				}
			};

			this.ofs += 0x18;

		}

		console.log(vertices);

		// Then we read the strip (fun)
		
		let sanity = true;
		this.ofs = polygon.stripOfs;

		console.log("Start Polygon offset: 0x%s", this.ofs.toString(16));

		do {

			let head = this.view.getUint16(this.ofs + 0x00, true);
			let flag = this.view.getUint16(this.ofs + 0x02, true);
			this.ofs += 0x04;

			console.log("HEAD: 0x%s", head.toString(16));
			console.log("FLAG: 0x%s", flag.toString(16));

			if(head === 0x8000 && flag === 0xFFFF) {
				break;
			}

			let stripCount = this.view.getInt16(this.ofs+ 0x00, true);
			stripCount = Math.abs(stripCount);
			console.log("Strip Count: %d", stripCount);
			this.ofs += 0x02;

			for(let i = 0; i < stripCount; i++) {

				let strip = [];
				let stripLen = this.view.getInt16(this.ofs + 0x00, true);
				stripLen = Math.abs(stripLen);
				console.log("Strip %d) Count: %d", i, stripLen);
				this.ofs += 2;

				for(let k = 0; k < stripLen; k++) {

					let point = {};
					point.index = this.view.getInt32(this.ofs + 0x00, true);
					this.ofs += 2;

					switch(head) {
					case 0x11:

						point.u0 = this.view.getInt16(this.view + 0x00, true) / 0x3ff;
						point.v0 = this.view.getInt16(this.view + 0x02, true) / 0x3ff;
						this.ofs += 0x04;


						break;
					case 0x13:

						point.u0 = this.view.getInt16(this.view + 0x00, true) / 0x3ff;
						point.v0 = this.view.getInt16(this.view + 0x02, true) / 0x3ff;
						point.u1 = this.view.getInt16(this.view + 0x04, true) / 0x3ff;
						point.v1 = this.view.getInt16(this.view + 0x06, true) / 0x3ff;
						this.ofs += 0x08;

						break;
					}
				
					strip.push(point);

				}
				
				this.appendStrip(vertices, strip);

			}

			if(this.ofs % 4) {
				this.ofs += 2;
			}
			
			sanity = false;

		} while(sanity);


	}

	appendStrip(vertices, strip) {

		console.log("appeding strip to geometry!!");

	}


}
