"use strict";

const fs = require('fs');
const src = fs.readFileSync('A_WALK_L_02.BIN');

// Read Header

console.log("----- READING HEADER -----");

const header = {
	flag : src.readUInt32LE(0x00),
	block1Ofs : 0x0c,
	block2Ofs : src.readUInt16LE(0x04),
	block3Ofs : src.readUInt16LE(0x06),
	block4Ofs : src.readUInt16LE(0x08),
	block5Ofs : src.readUInt16LE(0x0a)
};

console.log("Block 1 Ofs 0x%s", header.block1Ofs.toString(16));
console.log("Block 2 Ofs 0x%s", header.block2Ofs.toString(16));
console.log("Block 3 Ofs 0x%s", header.block3Ofs.toString(16));
console.log("Block 4 Ofs 0x%s", header.block4Ofs.toString(16));
console.log("Block 5 Ofs 0x%s", header.block5Ofs.toString(16));



// Block 1

console.log("----- READING BLOCK 1 -----");

const TRACK_DEF = []; 
let block_2_ofs = header.block2Ofs;
let block_3_ofs = header.block3Ofs;
let block_5_ofs = header.block5Ofs;

let zframe_count = 0;

for(let ofs = header.block1Ofs; ofs < header.block2Ofs; ofs += 2) {

	const instruction = src.readUInt16LE(ofs);

	if(instruction === 0) {
		break;
	}

	const def = {
		bone_id : instruction >> 9
	};
	TRACK_DEF.push(def);

	if(instruction & 0x1c0) {
		
		if(instruction & 0x100) {
			let frame_count = src.readUInt8(block_2_ofs++);

			let frames = [ 0 ];
			for(let i = 0; i < frame_count; i++) {
				frames.push(src.readUInt8(block_3_ofs++));
			}
			frames.push( 37 );
			zframe_count += frames.length;

			let values = [];
			frames.forEach(frame => {
				let ushort = src.readUInt16LE(block_5_ofs);
				block_5_ofs+= 2;
				values.push(decodeFloat16(ushort));
			});

			def.pos_x = {
				frame_count : frame_count + 2,
				frames : frames,
				values : values
			}
		}
		
		if(instruction & 0x80) {
			let frame_count = src.readUInt8(block_2_ofs++);
			let frames = [ 0 ];

			for(let i = 0; i < frame_count; i++) {
				frames.push(src.readUInt8(block_3_ofs++));
			}

			frames.push( 37 );
			zframe_count += frames.length;

			let values = [];
			frames.forEach(frame => {
				let ushort = src.readUInt16LE(block_5_ofs);
				block_5_ofs+= 2;
				values.push(decodeFloat16(ushort));
			});

			def.pos_y = {
				frame_count : frame_count + 2,
				frames : frames,
				values : values
			}
		}
		
		if(instruction & 0x40) {
			let frame_count = src.readUInt8(block_2_ofs++);
			let frames = [ 0 ];

			for(let i = 0; i < frame_count; i++) {
				frames.push(src.readUInt8(block_3_ofs++));
			}

			frames.push( 37 );
			zframe_count += frames.length;

			let values = [];
			frames.forEach(frame => {
				let ushort = src.readUInt16LE(block_5_ofs);
				block_5_ofs+= 2;
				values.push(decodeFloat16(ushort));
			});

			def.pos_z = {
				frame_count : frame_count + 2,
				frames : frames,
				values : values
			}
		}

	}

	if(instruction & 0x38) {
		
		if(instruction & 0x20) {
			let frame_count = src.readUInt8(block_2_ofs++);
			let frames = [ 0 ];

			for(let i = 0; i < frame_count; i++) {
				frames.push(src.readUInt8(block_3_ofs++));
			}

			frames.push( 37 );
			zframe_count += frames.length;

			let values = [];
			frames.forEach(frame => {
				let ushort = src.readUInt16LE(block_5_ofs);
				block_5_ofs+= 2;
				values.push(decodeFloat16(ushort));
			});

			def.rot_x = {
				frame_count : frame_count + 2,
				frames : frames,
				values : values
			}
		}
		
		if(instruction & 0x10) {
			let frame_count = src.readUInt8(block_2_ofs++);
			let frames = [ 0 ];

			for(let i = 0; i < frame_count; i++) {
				frames.push(src.readUInt8(block_3_ofs++));
			}

			frames.push( 37 );
			zframe_count += frames.length;

			let values = [];
			frames.forEach(frame => {
				let ushort = src.readUInt16LE(block_5_ofs);
				block_5_ofs+= 2;
				values.push(decodeFloat16(ushort));
			});

			def.rot_y = {
				frame_count : frame_count + 2,
				frames : frames,
				values : values
			}
		}
		
		if(instruction & 0x08) {

			let frame_count = src.readUInt8(block_2_ofs++);
			let frames = [ 0 ];

			for(let i = 0; i < frame_count; i++) {
				frames.push(src.readUInt8(block_3_ofs++));
			}

			frames.push( 37 );
			zframe_count += frames.length;

			let values = [];
			frames.forEach(frame => {
				let ushort = src.readUInt16LE(block_5_ofs);
				block_5_ofs+= 2;
				values.push(decodeFloat16(ushort));
			});

			def.rot_z = {
				frame_count : frame_count + 2,
				frames : frames,
				values : values
			}
		}

	}

}

let debug = JSON.stringify(TRACK_DEF, null, 4);
fs.writeFileSync("block_5.json", debug);

console.log("Block 5 position: 0x%s", block_5_ofs.toString(16));
console.log("File length: 0x%s", src.length.toString(16));

let block_5_len = src.length - header.block5Ofs;
let key_length = block_5_ofs - header.block5Ofs;

console.log("Coverage: %s", (key_length / block_5_len).toFixed(3));
console.log("Average: %s", (block_5_len / zframe_count).toFixed(3));

function decodeFloat16 (binary) {

    var exponent = (binary & 0x7C00) >> 10;
    var fraction = binary & 0x03FF;

    return (binary >> 15 ? -1 : 1) * (
        exponent ?
        (
            exponent === 0x1F ?
            fraction ? NaN : Infinity :
            Math.pow(2, exponent - 15) * (1 + fraction / 0x400)
        ) : 6.103515625e-5 * (fraction / 0x400)
    );

}

