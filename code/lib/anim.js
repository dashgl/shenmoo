const DATA = [
    {
        "Bone Name": "Root",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [
            {
                "frame": 0,
                "value": 1.11328125
            },
            {
                "frame": 5,
                "value": 1.1435546875
            },
            {
                "frame": 7,
                "value": 1.138671875
            },
            {
                "frame": 9,
                "value": 1.1376953125
            },
            {
                "frame": 16,
                "value": 1.1064453125
            },
            {
                "frame": 17,
                "value": 1.1064453125
            },
            {
                "frame": 19,
                "value": 1.1240234375
            },
            {
                "frame": 24,
                "value": 1.142578125
            },
            {
                "frame": 26,
                "value": 1.1376953125
            },
            {
                "frame": 27,
                "value": 1.12890625
            },
            {
                "frame": 30,
                "value": 1.1123046875
            },
            {
                "frame": 32,
                "value": 1.1044921875
            },
            {
                "frame": 35,
                "value": 1.1083984375
            },
            {
                "frame": 37,
                "value": 1.11328125
            }
        ],
        "Pos Z": [
            {
                "frame": 0,
                "value": -0.039947509765625
            },
            {
                "frame": 37,
                "value": -1.478515625
            }
        ],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Hip",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 85.82519531250021
            },
            {
                "frame": 18,
                "value": 93.51562500000023
            },
            {
                "frame": 37,
                "value": 85.82519531250021
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ]
    },
    {
        "Bone Name": "UpperLeg_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 7.465209960937519
            },
            {
                "frame": 37,
                "value": 7.465209960937519
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -0.6996917724609393
            },
            {
                "frame": 37,
                "value": -0.6996917724609393
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -1.9995117187500049
            },
            {
                "frame": 37,
                "value": -1.9995117187500049
            }
        ]
    },
    {
        "Bone Name": "FootIKTarget_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": 0.09759521484375
            },
            {
                "frame": 35,
                "value": 0.10009765625
            },
            {
                "frame": 37,
                "value": 0.09759521484375
            }
        ],
        "Pos Y": [
            {
                "frame": 0,
                "value": 0.239013671875
            },
            {
                "frame": 3,
                "value": 0.319580078125
            },
            {
                "frame": 7,
                "value": 0.255615234375
            },
            {
                "frame": 8,
                "value": 0.2384033203125
            },
            {
                "frame": 9,
                "value": 0.2188720703125
            },
            {
                "frame": 10,
                "value": 0.2012939453125
            },
            {
                "frame": 28,
                "value": 0.126953125
            },
            {
                "frame": 34,
                "value": 0.1920166015625
            },
            {
                "frame": 35,
                "value": 0.2103271484375
            },
            {
                "frame": 37,
                "value": 0.239013671875
            }
        ],
        "Pos Z": [
            {
                "frame": 0,
                "value": 0.3935546875
            },
            {
                "frame": 4,
                "value": 0.08905029296875
            },
            {
                "frame": 6,
                "value": -0.0953369140625
            },
            {
                "frame": 10,
                "value": -0.55615234375
            },
            {
                "frame": 13,
                "value": -0.859375
            },
            {
                "frame": 14,
                "value": -0.9208984375
            },
            {
                "frame": 18,
                "value": -0.962890625
            },
            {
                "frame": 19,
                "value": -0.96142578125
            },
            {
                "frame": 28,
                "value": -0.95751953125
            },
            {
                "frame": 31,
                "value": -0.958984375
            },
            {
                "frame": 33,
                "value": -0.9794921875
            },
            {
                "frame": 35,
                "value": -1.0244140625
            }
        ],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Foot_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 3,
                "value": -63.852539062500156
            },
            {
                "frame": 17,
                "value": 2.480163574218756
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -9.997558593750025
            },
            {
                "frame": 37,
                "value": -9.997558593750025
            }
        ],
        "Rot Z": []
    },
    {
        "Bone Name": "UpperLeg_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -7.542114257812519
            },
            {
                "frame": 37,
                "value": -7.542114257812519
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 0.6996917724609393
            },
            {
                "frame": 37,
                "value": 0.6996917724609393
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 1.9995117187500049
            },
            {
                "frame": 37,
                "value": 1.9995117187500049
            }
        ]
    },
    {
        "Bone Name": "FootIKTarget_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": -0.10150146484375
            },
            {
                "frame": 21,
                "value": -0.10400390625
            },
            {
                "frame": 37,
                "value": -0.10150146484375
            }
        ],
        "Pos Y": [
            {
                "frame": 0,
                "value": 0.116943359375
            },
            {
                "frame": 5,
                "value": 0.11444091796875
            },
            {
                "frame": 11,
                "value": 0.1396484375
            },
            {
                "frame": 16,
                "value": 0.1737060546875
            },
            {
                "frame": 21,
                "value": 0.302001953125
            },
            {
                "frame": 25,
                "value": 0.2415771484375
            },
            {
                "frame": 27,
                "value": 0.211669921875
            },
            {
                "frame": 30,
                "value": 0.1640625
            }
        ],
        "Pos Z": [
            {
                "frame": 0,
                "value": -0.245361328125
            },
            {
                "frame": 1,
                "value": -0.254638671875
            },
            {
                "frame": 2,
                "value": -0.254638671875
            },
            {
                "frame": 8,
                "value": -0.254638671875
            },
            {
                "frame": 10,
                "value": -0.25634765625
            },
            {
                "frame": 13,
                "value": -0.27490234375
            },
            {
                "frame": 16,
                "value": -0.31884765625
            },
            {
                "frame": 18,
                "value": -0.359619140625
            },
            {
                "frame": 27,
                "value": -1.1142578125
            },
            {
                "frame": 31,
                "value": -1.5322265625
            },
            {
                "frame": 33,
                "value": -1.625
            },
            {
                "frame": 35,
                "value": -1.6728515625
            }
        ],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Foot_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 1.9995117187500049
            },
            {
                "frame": 35,
                "value": 2.3112487792968808
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 9.997558593750025
            },
            {
                "frame": 37,
                "value": 9.997558593750025
            }
        ],
        "Rot Z": []
    },
    {
        "Bone Name": "Torso",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [
            {
                "frame": 0,
                "value": 97.03125000000024
            },
            {
                "frame": 18,
                "value": 81.6943359375002
            },
            {
                "frame": 37,
                "value": 97.03125000000024
            }
        ],
        "Rot Z": []
    },
    {
        "Bone Name": "UpperTorsoIKTarget",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": -0.00482940673828125
            },
            {
                "frame": 17,
                "value": 0.00403594970703125
            },
            {
                "frame": 37,
                "value": -0.00482940673828125
            }
        ],
        "Pos Y": [
            {
                "frame": 5,
                "value": 1.513671875
            },
            {
                "frame": 16,
                "value": 1.4755859375
            },
            {
                "frame": 23,
                "value": 1.513671875
            },
            {
                "frame": 34,
                "value": 1.4765625
            }
        ],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Neck",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -89.95605468750023
            },
            {
                "frame": 37,
                "value": -89.95605468750023
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 15.996093750000039
            },
            {
                "frame": 37,
                "value": 15.996093750000039
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -90.00000000000023
            },
            {
                "frame": 37,
                "value": -90.00000000000023
            }
        ]
    },
    {
        "Bone Name": "HeadLookAtTarget",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": -0.00482940673828125
            },
            {
                "frame": 17,
                "value": 0.00403594970703125
            },
            {
                "frame": 37,
                "value": -0.00482940673828125
            }
        ],
        "Pos Y": [
            {
                "frame": 5,
                "value": 1.4794921875
            },
            {
                "frame": 16,
                "value": 1.44140625
            },
            {
                "frame": 23,
                "value": 1.4794921875
            },
            {
                "frame": 34,
                "value": 1.4423828125
            }
        ],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Shoulder_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 0.9832763671875024
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 1.8965148925781297
            },
            {
                "frame": 19,
                "value": -1.8704223632812547
            },
            {
                "frame": 37,
                "value": 1.8965148925781297
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 2.03933715820313
            },
            {
                "frame": 34,
                "value": 1.8058776855468794
            },
            {
                "frame": 37,
                "value": 2.03933715820313
            }
        ]
    },
    {
        "Bone Name": "Arm_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 19.58862304687505
            },
            {
                "frame": 37,
                "value": 19.58862304687505
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 11.95312500000003
            },
            {
                "frame": 7,
                "value": 5.614013671875014
            },
            {
                "frame": 22,
                "value": 3.5183715820312584
            },
            {
                "frame": 29,
                "value": 10.074462890625025
            },
            {
                "frame": 35,
                "value": 12.854003906250032
            },
            {
                "frame": 37,
                "value": 11.95312500000003
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -184.57031250000045
            },
            {
                "frame": 2,
                "value": -182.46093750000045
            },
            {
                "frame": 13,
                "value": -147.56835937500037
            },
            {
                "frame": 15,
                "value": -147.04101562500037
            },
            {
                "frame": 18,
                "value": -147.39257812500037
            },
            {
                "frame": 32,
                "value": -184.21875000000045
            },
            {
                "frame": 37,
                "value": -184.57031250000045
            }
        ]
    },
    {
        "Bone Name": "HandIKTarget_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 37,
                "value": -0.257568359375
            }
        ],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Hand_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -0.46176910400390736
            },
            {
                "frame": 33,
                "value": 3.8314819335937593
            },
            {
                "frame": 37,
                "value": -0.46176910400390736
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -5.745849609375014
            },
            {
                "frame": 16,
                "value": 1.9267272949218797
            },
            {
                "frame": 35,
                "value": -6.119384765625015
            },
            {
                "frame": 37,
                "value": -5.745849609375014
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ]
    },
    {
        "Bone Name": "Shoulder_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [
            {
                "frame": 18,
                "value": -1.8965148925781297
            }
        ],
        "Rot Z": [
            {
                "frame": 16,
                "value": 1.8058776855468794
            },
            {
                "frame": 18,
                "value": 2.03933715820313
            }
        ]
    },
    {
        "Bone Name": "Arm_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -0.15612602233886758
            },
            {
                "frame": 16,
                "value": -13.480224609375034
            },
            {
                "frame": 37,
                "value": -0.15612602233886758
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -5.295410156250013
            },
            {
                "frame": 16,
                "value": -15.161132812500037
            },
            {
                "frame": 23,
                "value": -9.799804687500025
            },
            {
                "frame": 32,
                "value": -8.05847167968752
            },
            {
                "frame": 37,
                "value": -5.295410156250013
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -158.7304687500004
            },
            {
                "frame": 4,
                "value": -163.1250000000004
            },
            {
                "frame": 15,
                "value": -197.75390625000048
            },
            {
                "frame": 17,
                "value": -198.28125000000048
            },
            {
                "frame": 20,
                "value": -197.75390625000048
            },
            {
                "frame": 33,
                "value": -159.6093750000004
            },
            {
                "frame": 37,
                "value": -158.7304687500004
            }
        ]
    },
    {
        "Bone Name": "Unknown35",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 37,
                "value": 0.2474365234375
            }
        ],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Hand_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 15,
                "value": -3.8314819335937593
            }
        ],
        "Rot Y": [
            {
                "frame": 17,
                "value": 6.119384765625015
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ]
    },
    {
        "Bone Name": "Root",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [
            {
                "frame": 0,
                "value": 1.11328125
            },
            {
                "frame": 5,
                "value": 1.1435546875
            },
            {
                "frame": 7,
                "value": 1.138671875
            },
            {
                "frame": 9,
                "value": 1.1376953125
            },
            {
                "frame": 16,
                "value": 1.1064453125
            },
            {
                "frame": 17,
                "value": 1.1064453125
            },
            {
                "frame": 19,
                "value": 1.1240234375
            },
            {
                "frame": 24,
                "value": 1.142578125
            },
            {
                "frame": 26,
                "value": 1.1376953125
            },
            {
                "frame": 27,
                "value": 1.12890625
            },
            {
                "frame": 30,
                "value": 1.1123046875
            },
            {
                "frame": 32,
                "value": 1.1044921875
            },
            {
                "frame": 35,
                "value": 1.1083984375
            },
            {
                "frame": 37,
                "value": 1.11328125
            }
        ],
        "Pos Z": [
            {
                "frame": 0,
                "value": -0.039947509765625
            },
            {
                "frame": 37,
                "value": -1.478515625
            }
        ],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Hip",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 85.82519531250021
            },
            {
                "frame": 18,
                "value": 93.51562500000023
            },
            {
                "frame": 37,
                "value": 85.82519531250021
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ]
    },
    {
        "Bone Name": "UpperLeg_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 7.465209960937519
            },
            {
                "frame": 37,
                "value": 7.465209960937519
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -0.6996917724609393
            },
            {
                "frame": 37,
                "value": -0.6996917724609393
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -1.9995117187500049
            },
            {
                "frame": 37,
                "value": -1.9995117187500049
            }
        ]
    },
    {
        "Bone Name": "FootIKTarget_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": 0.09759521484375
            },
            {
                "frame": 35,
                "value": 0.10009765625
            },
            {
                "frame": 37,
                "value": 0.09759521484375
            }
        ],
        "Pos Y": [
            {
                "frame": 0,
                "value": 0.239013671875
            },
            {
                "frame": 3,
                "value": 0.319580078125
            },
            {
                "frame": 7,
                "value": 0.255615234375
            },
            {
                "frame": 8,
                "value": 0.2384033203125
            },
            {
                "frame": 9,
                "value": 0.2188720703125
            },
            {
                "frame": 10,
                "value": 0.2012939453125
            },
            {
                "frame": 28,
                "value": 0.126953125
            },
            {
                "frame": 34,
                "value": 0.1920166015625
            },
            {
                "frame": 35,
                "value": 0.2103271484375
            },
            {
                "frame": 37,
                "value": 0.239013671875
            }
        ],
        "Pos Z": [
            {
                "frame": 0,
                "value": 0.3935546875
            },
            {
                "frame": 4,
                "value": 0.08905029296875
            },
            {
                "frame": 6,
                "value": -0.0953369140625
            },
            {
                "frame": 10,
                "value": -0.55615234375
            },
            {
                "frame": 13,
                "value": -0.859375
            },
            {
                "frame": 14,
                "value": -0.9208984375
            },
            {
                "frame": 18,
                "value": -0.962890625
            },
            {
                "frame": 19,
                "value": -0.96142578125
            },
            {
                "frame": 28,
                "value": -0.95751953125
            },
            {
                "frame": 31,
                "value": -0.958984375
            },
            {
                "frame": 33,
                "value": -0.9794921875
            },
            {
                "frame": 35,
                "value": -1.0244140625
            }
        ],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Foot_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 3,
                "value": -63.852539062500156
            },
            {
                "frame": 17,
                "value": 2.480163574218756
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -9.997558593750025
            },
            {
                "frame": 37,
                "value": -9.997558593750025
            }
        ],
        "Rot Z": []
    },
    {
        "Bone Name": "UpperLeg_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -7.542114257812519
            },
            {
                "frame": 37,
                "value": -7.542114257812519
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 0.6996917724609393
            },
            {
                "frame": 37,
                "value": 0.6996917724609393
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 1.9995117187500049
            },
            {
                "frame": 37,
                "value": 1.9995117187500049
            }
        ]
    },
    {
        "Bone Name": "FootIKTarget_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": -0.10150146484375
            },
            {
                "frame": 21,
                "value": -0.10400390625
            },
            {
                "frame": 37,
                "value": -0.10150146484375
            }
        ],
        "Pos Y": [
            {
                "frame": 0,
                "value": 0.116943359375
            },
            {
                "frame": 5,
                "value": 0.11444091796875
            },
            {
                "frame": 11,
                "value": 0.1396484375
            },
            {
                "frame": 16,
                "value": 0.1737060546875
            },
            {
                "frame": 21,
                "value": 0.302001953125
            },
            {
                "frame": 25,
                "value": 0.2415771484375
            },
            {
                "frame": 27,
                "value": 0.211669921875
            },
            {
                "frame": 30,
                "value": 0.1640625
            }
        ],
        "Pos Z": [
            {
                "frame": 0,
                "value": -0.245361328125
            },
            {
                "frame": 1,
                "value": -0.254638671875
            },
            {
                "frame": 2,
                "value": -0.254638671875
            },
            {
                "frame": 8,
                "value": -0.254638671875
            },
            {
                "frame": 10,
                "value": -0.25634765625
            },
            {
                "frame": 13,
                "value": -0.27490234375
            },
            {
                "frame": 16,
                "value": -0.31884765625
            },
            {
                "frame": 18,
                "value": -0.359619140625
            },
            {
                "frame": 27,
                "value": -1.1142578125
            },
            {
                "frame": 31,
                "value": -1.5322265625
            },
            {
                "frame": 33,
                "value": -1.625
            },
            {
                "frame": 35,
                "value": -1.6728515625
            }
        ],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Foot_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 1.9995117187500049
            },
            {
                "frame": 35,
                "value": 2.3112487792968808
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 9.997558593750025
            },
            {
                "frame": 37,
                "value": 9.997558593750025
            }
        ],
        "Rot Z": []
    },
    {
        "Bone Name": "Torso",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [
            {
                "frame": 0,
                "value": 97.03125000000024
            },
            {
                "frame": 18,
                "value": 81.6943359375002
            },
            {
                "frame": 37,
                "value": 97.03125000000024
            }
        ],
        "Rot Z": []
    },
    {
        "Bone Name": "UpperTorsoIKTarget",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": -0.00482940673828125
            },
            {
                "frame": 17,
                "value": 0.00403594970703125
            },
            {
                "frame": 37,
                "value": -0.00482940673828125
            }
        ],
        "Pos Y": [
            {
                "frame": 5,
                "value": 1.513671875
            },
            {
                "frame": 16,
                "value": 1.4755859375
            },
            {
                "frame": 23,
                "value": 1.513671875
            },
            {
                "frame": 34,
                "value": 1.4765625
            }
        ],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Neck",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -89.95605468750023
            },
            {
                "frame": 37,
                "value": -89.95605468750023
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 15.996093750000039
            },
            {
                "frame": 37,
                "value": 15.996093750000039
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -90.00000000000023
            },
            {
                "frame": 37,
                "value": -90.00000000000023
            }
        ]
    },
    {
        "Bone Name": "HeadLookAtTarget",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": -0.00482940673828125
            },
            {
                "frame": 17,
                "value": 0.00403594970703125
            },
            {
                "frame": 37,
                "value": -0.00482940673828125
            }
        ],
        "Pos Y": [
            {
                "frame": 5,
                "value": 1.4794921875
            },
            {
                "frame": 16,
                "value": 1.44140625
            },
            {
                "frame": 23,
                "value": 1.4794921875
            },
            {
                "frame": 34,
                "value": 1.4423828125
            }
        ],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Shoulder_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 0.9832763671875024
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 1.8965148925781297
            },
            {
                "frame": 19,
                "value": -1.8704223632812547
            },
            {
                "frame": 37,
                "value": 1.8965148925781297
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 2.03933715820313
            },
            {
                "frame": 34,
                "value": 1.8058776855468794
            },
            {
                "frame": 37,
                "value": 2.03933715820313
            }
        ]
    },
    {
        "Bone Name": "Arm_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 19.58862304687505
            },
            {
                "frame": 37,
                "value": 19.58862304687505
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 11.95312500000003
            },
            {
                "frame": 7,
                "value": 5.614013671875014
            },
            {
                "frame": 22,
                "value": 3.5183715820312584
            },
            {
                "frame": 29,
                "value": 10.074462890625025
            },
            {
                "frame": 35,
                "value": 12.854003906250032
            },
            {
                "frame": 37,
                "value": 11.95312500000003
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -184.57031250000045
            },
            {
                "frame": 2,
                "value": -182.46093750000045
            },
            {
                "frame": 13,
                "value": -147.56835937500037
            },
            {
                "frame": 15,
                "value": -147.04101562500037
            },
            {
                "frame": 18,
                "value": -147.39257812500037
            },
            {
                "frame": 32,
                "value": -184.21875000000045
            },
            {
                "frame": 37,
                "value": -184.57031250000045
            }
        ]
    },
    {
        "Bone Name": "HandIKTarget_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 37,
                "value": -0.257568359375
            }
        ],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Hand_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -0.46176910400390736
            },
            {
                "frame": 33,
                "value": 3.8314819335937593
            },
            {
                "frame": 37,
                "value": -0.46176910400390736
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -5.745849609375014
            },
            {
                "frame": 16,
                "value": 1.9267272949218797
            },
            {
                "frame": 35,
                "value": -6.119384765625015
            },
            {
                "frame": 37,
                "value": -5.745849609375014
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ]
    },
    {
        "Bone Name": "Shoulder_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [
            {
                "frame": 18,
                "value": -1.8965148925781297
            }
        ],
        "Rot Z": [
            {
                "frame": 16,
                "value": 1.8058776855468794
            },
            {
                "frame": 18,
                "value": 2.03933715820313
            }
        ]
    },
    {
        "Bone Name": "Arm_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -0.15612602233886758
            },
            {
                "frame": 16,
                "value": -13.480224609375034
            },
            {
                "frame": 37,
                "value": -0.15612602233886758
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -5.295410156250013
            },
            {
                "frame": 16,
                "value": -15.161132812500037
            },
            {
                "frame": 23,
                "value": -9.799804687500025
            },
            {
                "frame": 32,
                "value": -8.05847167968752
            },
            {
                "frame": 37,
                "value": -5.295410156250013
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -158.7304687500004
            },
            {
                "frame": 4,
                "value": -163.1250000000004
            },
            {
                "frame": 15,
                "value": -197.75390625000048
            },
            {
                "frame": 17,
                "value": -198.28125000000048
            },
            {
                "frame": 20,
                "value": -197.75390625000048
            },
            {
                "frame": 33,
                "value": -159.6093750000004
            },
            {
                "frame": 37,
                "value": -158.7304687500004
            }
        ]
    },
    {
        "Bone Name": "Unknown35",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 37,
                "value": 0.2474365234375
            }
        ],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Hand_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 15,
                "value": -3.8314819335937593
            }
        ],
        "Rot Y": [
            {
                "frame": 17,
                "value": 6.119384765625015
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ]
    },
    {
        "Bone Name": "Root",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [
            {
                "frame": 0,
                "value": 1.11328125
            },
            {
                "frame": 5,
                "value": 1.1435546875
            },
            {
                "frame": 7,
                "value": 1.138671875
            },
            {
                "frame": 9,
                "value": 1.1376953125
            },
            {
                "frame": 16,
                "value": 1.1064453125
            },
            {
                "frame": 17,
                "value": 1.1064453125
            },
            {
                "frame": 19,
                "value": 1.1240234375
            },
            {
                "frame": 24,
                "value": 1.142578125
            },
            {
                "frame": 26,
                "value": 1.1376953125
            },
            {
                "frame": 27,
                "value": 1.12890625
            },
            {
                "frame": 30,
                "value": 1.1123046875
            },
            {
                "frame": 32,
                "value": 1.1044921875
            },
            {
                "frame": 35,
                "value": 1.1083984375
            },
            {
                "frame": 37,
                "value": 1.11328125
            }
        ],
        "Pos Z": [
            {
                "frame": 0,
                "value": -0.039947509765625
            },
            {
                "frame": 37,
                "value": -1.478515625
            }
        ],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Hip",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 85.82519531250021
            },
            {
                "frame": 18,
                "value": 93.51562500000023
            },
            {
                "frame": 37,
                "value": 85.82519531250021
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ]
    },
    {
        "Bone Name": "UpperLeg_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 7.465209960937519
            },
            {
                "frame": 37,
                "value": 7.465209960937519
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -0.6996917724609393
            },
            {
                "frame": 37,
                "value": -0.6996917724609393
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -1.9995117187500049
            },
            {
                "frame": 37,
                "value": -1.9995117187500049
            }
        ]
    },
    {
        "Bone Name": "FootIKTarget_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": 0.09759521484375
            },
            {
                "frame": 35,
                "value": 0.10009765625
            },
            {
                "frame": 37,
                "value": 0.09759521484375
            }
        ],
        "Pos Y": [
            {
                "frame": 0,
                "value": 0.239013671875
            },
            {
                "frame": 3,
                "value": 0.319580078125
            },
            {
                "frame": 7,
                "value": 0.255615234375
            },
            {
                "frame": 8,
                "value": 0.2384033203125
            },
            {
                "frame": 9,
                "value": 0.2188720703125
            },
            {
                "frame": 10,
                "value": 0.2012939453125
            },
            {
                "frame": 28,
                "value": 0.126953125
            },
            {
                "frame": 34,
                "value": 0.1920166015625
            },
            {
                "frame": 35,
                "value": 0.2103271484375
            },
            {
                "frame": 37,
                "value": 0.239013671875
            }
        ],
        "Pos Z": [
            {
                "frame": 0,
                "value": 0.3935546875
            },
            {
                "frame": 4,
                "value": 0.08905029296875
            },
            {
                "frame": 6,
                "value": -0.0953369140625
            },
            {
                "frame": 10,
                "value": -0.55615234375
            },
            {
                "frame": 13,
                "value": -0.859375
            },
            {
                "frame": 14,
                "value": -0.9208984375
            },
            {
                "frame": 18,
                "value": -0.962890625
            },
            {
                "frame": 19,
                "value": -0.96142578125
            },
            {
                "frame": 28,
                "value": -0.95751953125
            },
            {
                "frame": 31,
                "value": -0.958984375
            },
            {
                "frame": 33,
                "value": -0.9794921875
            },
            {
                "frame": 35,
                "value": -1.0244140625
            }
        ],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Foot_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 3,
                "value": -63.852539062500156
            },
            {
                "frame": 17,
                "value": 2.480163574218756
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -9.997558593750025
            },
            {
                "frame": 37,
                "value": -9.997558593750025
            }
        ],
        "Rot Z": []
    },
    {
        "Bone Name": "UpperLeg_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -7.542114257812519
            },
            {
                "frame": 37,
                "value": -7.542114257812519
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 0.6996917724609393
            },
            {
                "frame": 37,
                "value": 0.6996917724609393
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 1.9995117187500049
            },
            {
                "frame": 37,
                "value": 1.9995117187500049
            }
        ]
    },
    {
        "Bone Name": "FootIKTarget_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": -0.10150146484375
            },
            {
                "frame": 21,
                "value": -0.10400390625
            },
            {
                "frame": 37,
                "value": -0.10150146484375
            }
        ],
        "Pos Y": [
            {
                "frame": 0,
                "value": 0.116943359375
            },
            {
                "frame": 5,
                "value": 0.11444091796875
            },
            {
                "frame": 11,
                "value": 0.1396484375
            },
            {
                "frame": 16,
                "value": 0.1737060546875
            },
            {
                "frame": 21,
                "value": 0.302001953125
            },
            {
                "frame": 25,
                "value": 0.2415771484375
            },
            {
                "frame": 27,
                "value": 0.211669921875
            },
            {
                "frame": 30,
                "value": 0.1640625
            }
        ],
        "Pos Z": [
            {
                "frame": 0,
                "value": -0.245361328125
            },
            {
                "frame": 1,
                "value": -0.254638671875
            },
            {
                "frame": 2,
                "value": -0.254638671875
            },
            {
                "frame": 8,
                "value": -0.254638671875
            },
            {
                "frame": 10,
                "value": -0.25634765625
            },
            {
                "frame": 13,
                "value": -0.27490234375
            },
            {
                "frame": 16,
                "value": -0.31884765625
            },
            {
                "frame": 18,
                "value": -0.359619140625
            },
            {
                "frame": 27,
                "value": -1.1142578125
            },
            {
                "frame": 31,
                "value": -1.5322265625
            },
            {
                "frame": 33,
                "value": -1.625
            },
            {
                "frame": 35,
                "value": -1.6728515625
            }
        ],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Foot_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 1.9995117187500049
            },
            {
                "frame": 35,
                "value": 2.3112487792968808
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 9.997558593750025
            },
            {
                "frame": 37,
                "value": 9.997558593750025
            }
        ],
        "Rot Z": []
    },
    {
        "Bone Name": "Torso",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [
            {
                "frame": 0,
                "value": 97.03125000000024
            },
            {
                "frame": 18,
                "value": 81.6943359375002
            },
            {
                "frame": 37,
                "value": 97.03125000000024
            }
        ],
        "Rot Z": []
    },
    {
        "Bone Name": "UpperTorsoIKTarget",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": -0.00482940673828125
            },
            {
                "frame": 17,
                "value": 0.00403594970703125
            },
            {
                "frame": 37,
                "value": -0.00482940673828125
            }
        ],
        "Pos Y": [
            {
                "frame": 5,
                "value": 1.513671875
            },
            {
                "frame": 16,
                "value": 1.4755859375
            },
            {
                "frame": 23,
                "value": 1.513671875
            },
            {
                "frame": 34,
                "value": 1.4765625
            }
        ],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Neck",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -89.95605468750023
            },
            {
                "frame": 37,
                "value": -89.95605468750023
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 15.996093750000039
            },
            {
                "frame": 37,
                "value": 15.996093750000039
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -90.00000000000023
            },
            {
                "frame": 37,
                "value": -90.00000000000023
            }
        ]
    },
    {
        "Bone Name": "HeadLookAtTarget",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 0,
                "value": -0.00482940673828125
            },
            {
                "frame": 17,
                "value": 0.00403594970703125
            },
            {
                "frame": 37,
                "value": -0.00482940673828125
            }
        ],
        "Pos Y": [
            {
                "frame": 5,
                "value": 1.4794921875
            },
            {
                "frame": 16,
                "value": 1.44140625
            },
            {
                "frame": 23,
                "value": 1.4794921875
            },
            {
                "frame": 34,
                "value": 1.4423828125
            }
        ],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Shoulder_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 0.9832763671875024
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 1.8965148925781297
            },
            {
                "frame": 19,
                "value": -1.8704223632812547
            },
            {
                "frame": 37,
                "value": 1.8965148925781297
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 2.03933715820313
            },
            {
                "frame": 34,
                "value": 1.8058776855468794
            },
            {
                "frame": 37,
                "value": 2.03933715820313
            }
        ]
    },
    {
        "Bone Name": "Arm_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": 19.58862304687505
            },
            {
                "frame": 37,
                "value": 19.58862304687505
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": 11.95312500000003
            },
            {
                "frame": 7,
                "value": 5.614013671875014
            },
            {
                "frame": 22,
                "value": 3.5183715820312584
            },
            {
                "frame": 29,
                "value": 10.074462890625025
            },
            {
                "frame": 35,
                "value": 12.854003906250032
            },
            {
                "frame": 37,
                "value": 11.95312500000003
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -184.57031250000045
            },
            {
                "frame": 2,
                "value": -182.46093750000045
            },
            {
                "frame": 13,
                "value": -147.56835937500037
            },
            {
                "frame": 15,
                "value": -147.04101562500037
            },
            {
                "frame": 18,
                "value": -147.39257812500037
            },
            {
                "frame": 32,
                "value": -184.21875000000045
            },
            {
                "frame": 37,
                "value": -184.57031250000045
            }
        ]
    },
    {
        "Bone Name": "HandIKTarget_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 37,
                "value": -0.257568359375
            }
        ],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Hand_R",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -0.46176910400390736
            },
            {
                "frame": 33,
                "value": 3.8314819335937593
            },
            {
                "frame": 37,
                "value": -0.46176910400390736
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -5.745849609375014
            },
            {
                "frame": 16,
                "value": 1.9267272949218797
            },
            {
                "frame": 35,
                "value": -6.119384765625015
            },
            {
                "frame": 37,
                "value": -5.745849609375014
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ]
    },
    {
        "Bone Name": "Shoulder_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [
            {
                "frame": 18,
                "value": -1.8965148925781297
            }
        ],
        "Rot Z": [
            {
                "frame": 16,
                "value": 1.8058776855468794
            },
            {
                "frame": 18,
                "value": 2.03933715820313
            }
        ]
    },
    {
        "Bone Name": "Arm_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 0,
                "value": -0.15612602233886758
            },
            {
                "frame": 16,
                "value": -13.480224609375034
            },
            {
                "frame": 37,
                "value": -0.15612602233886758
            }
        ],
        "Rot Y": [
            {
                "frame": 0,
                "value": -5.295410156250013
            },
            {
                "frame": 16,
                "value": -15.161132812500037
            },
            {
                "frame": 23,
                "value": -9.799804687500025
            },
            {
                "frame": 32,
                "value": -8.05847167968752
            },
            {
                "frame": 37,
                "value": -5.295410156250013
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": -158.7304687500004
            },
            {
                "frame": 4,
                "value": -163.1250000000004
            },
            {
                "frame": 15,
                "value": -197.75390625000048
            },
            {
                "frame": 17,
                "value": -198.28125000000048
            },
            {
                "frame": 20,
                "value": -197.75390625000048
            },
            {
                "frame": 33,
                "value": -159.6093750000004
            },
            {
                "frame": 37,
                "value": -158.7304687500004
            }
        ]
    },
    {
        "Bone Name": "Unknown35",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [
            {
                "frame": 37,
                "value": 0.2474365234375
            }
        ],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [],
        "Rot Y": [],
        "Rot Z": []
    },
    {
        "Bone Name": "Hand_L",
        "Rig Id": null,
        "MD5 Id": null,
        "Pos X": [],
        "Pos Y": [],
        "Pos Z": [],
        "Rot X": [
            {
                "frame": 15,
                "value": -3.8314819335937593
            }
        ],
        "Rot Y": [
            {
                "frame": 17,
                "value": 6.119384765625015
            }
        ],
        "Rot Z": [
            {
                "frame": 0,
                "value": 0.010986328125000028
            },
            {
                "frame": 37,
                "value": 0.010986328125000028
            }
        ]
    }
]
