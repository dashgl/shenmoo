"use strict";

const http = require('http');
const fs = require('fs');
const path = require('path');

http.createServer(function(req, res) {

	let filepath = './public' + req.url;
	if(filepath === './public/') {
		filepath += 'index.html';
	}

	let contentType = 'application/octet-stream';

	let ext = path.extname(filepath);
	switch(ext) {
	case '.js':
		contentType = 'text/javascript';
		break;
	case '.html':
		contentType = 'text/html';
		break;
	case '.css':
		contentType = 'text/css';
		break;
	}

	fs.readFile(filepath, function(err, data) {
		if(err) {
			res.writeHead(404, {'Content-Type' : 'text/html'});
			return res.end('File not found');
		}

		res.writeHead(200, {'Content-Type' : contentType});
		res.end(data);
	});

}).listen(8888);
