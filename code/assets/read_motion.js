"use strict";

const fs = require('fs');
const src = fs.readFileSync('A_WALK_L_02.BIN');

// Read Header

console.log("----- READING HEADER -----");

const header = {
	flag : src.readUInt32LE(0x00),
	block1Ofs : 0x0c,
	block2Ofs : src.readUInt16LE(0x04),
	block3Ofs : src.readUInt16LE(0x06),
	block4Ofs : src.readUInt16LE(0x08),
	block5Ofs : src.readUInt16LE(0x0a)
};

console.log("Block 1 Ofs 0x%s", header.block1Ofs.toString(16));
console.log("Block 2 Ofs 0x%s", header.block2Ofs.toString(16));
console.log("Block 3 Ofs 0x%s", header.block3Ofs.toString(16));
console.log("Block 4 Ofs 0x%s", header.block4Ofs.toString(16));
console.log("Block 5 Ofs 0x%s", header.block5Ofs.toString(16));

// Block 1

console.log("----- READING BLOCK 1 -----");

let stride = 0;
const TRACK_DEF = []; 
for(let ofs = header.block1Ofs; ofs < header.block2Ofs; ofs += 2) {

	const instruction = src.readUInt16LE(ofs);

	if(instruction === 0) {
		break;
	}

	console.log("Instruction: 0x%s", instruction.toString(16));
	console.log("Bone Id: %d", instruction >> 9);

	TRACK_DEF.push({
		bone_id : instruction >> 9,
		pos_x : instruction && 0x100 ? 1 : 0,
		pos_y : instruction && 0x80 ? 1 : 0,
		pos_z : instruction && 0x40 ? 1 : 0,
		rot_x : instruction && 0x20 ? 1 : 0,
		rot_y : instruction && 0x10 ? 1 : 0,
		rot_z : instruction && 0x08 ? 1 : 0
	});

	if(instruction && 0x100) {
		console.log("pos x");
		stride += 2;
	}

	if(instruction && 0x80) {
		console.log("pos y");
		stride += 2;
	}

	if(instruction && 0x40) {
		console.log("pos z");
		stride += 2;
	}

	if(instruction && 0x20) {
		console.log("rot x");
		stride += 2;
	}

	if(instruction && 0x10) {
		console.log("rot y");
		stride += 2;
	}

	if(instruction && 0x08) {
		console.log("rot z");
		stride += 2;
	}

}

console.log("Full stride: 0x%s", stride.toString(16));

// Block 5

console.log("----- READING BLOCK 5 -----");

const block5_len = src.length - header.block5Ofs;
const frame_count = Math.floor(block_len / stride);

let ofs = header.block5Ofs;
for(let i = 0; i < frame_count; i++) {
	
	TRACK_DEF.forEach(def => {

		let track = {
			bone_id : def.bone_id
		}
		
		if(def.pos_x) {
			track.pos_x = src.readUInt16LE(ofs);
			ofs+ = 2;
		}
		
		if(def.pos_y) {
			track.pos_y = src.readUInt16LE(ofs);
			ofs+ = 2;
		}
		
		if(def.pos_z) {
			track.pos_z = src.readUInt16LE(ofs);
			ofs+ = 2;
		}
		
		if(def.rot_x) {
			track.rot_x = src.readUInt16LE(ofs);
			ofs+ = 2;
		}
		
		if(def.rot_y) {
			track.rot_y = src.readUInt16LE(ofs);
			ofs+ = 2;
		}
		
		if(def.rot_z) {
			track.rot_z = src.readUInt16LE(ofs);
			ofs+ = 2;
		}

	});

}

