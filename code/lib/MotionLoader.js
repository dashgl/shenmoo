"use strict";

THREE.MotionLoader = class {

	constructor(mesh, buffer) {

		this.bones = mesh.skeleton.bones;
		this.view = new DataView(buffer);
		this.length = buffer.byteLength;

		this.mem = {};

	}

	parse(bones) {

		this.mem.anim_length = this.view.getUint16(0x00, true);

		this.mem.block1Ofs = 0x0c;
		this.mem.block2Ofs = this.view.getUint16(0x04, true);
		this.mem.block3Ofs = this.view.getUint16(0x06, true);
		this.mem.block4Ofs = this.view.getUint16(0x08, true);
		this.mem.block5Ofs = this.view.getUint16(0x0a, true);

		this.mem.block_2_half = this.mem.anim_length & 0xffff8000 >= 0;
		this.mem.block_3_half = this.mem.anim_length & 0x7fff <= 0xff;
		this.mem.block_2_half = true;
		this.mem.block_3_half = true;

		const pos_mask = 0x1c0;
		const rot_mask = 0x38;
		const block1End = this.mem.block2Ofs;
		this.mem.b4_mask = 0xc0;

		const nodes = [];
		for(let ofs = this.mem.block1Ofs; ofs < block1End; ofs += 2) {
			
			const instruction = this.view.getUint16(ofs, true);
			if(instruction === 0) {
				break;
			}
		
			const node = {
				id : instruction >> 9
			};

			if(node.id === 8) {
				console.log("0x%s", instruction.toString(16));
			}

			if(instruction & pos_mask) {

				if(instruction & 0x100) {
					this.readKeyValues(node, 'pos', 'x');
				}

				if(instruction & 0x80) {
					this.readKeyValues(node, 'pos', 'y');
				}

				if(instruction & 0x40) {
					this.readKeyValues(node, 'pos', 'z');
				}

			}

			if(instruction & rot_mask) {

				if(instruction & 0x20) {
					this.readKeyValues(node, 'rot', 'x');
				}

				if(instruction & 0x10) {
					this.readKeyValues(node, 'rot', 'y');
				}

				if(instruction & 0x08) {
					this.readKeyValues(node, 'rot', 'z');
				}

			}

			nodes.push(node);
			
		}
		
		console.log(" ---- ");
		console.log(nodes);
		console.log(" ---- ");
		return;

		const name = "AKI_WALK";
		const duration = 37 / 30;
		const tracks = [];

		const bone_lookup = [
			{
				// pos , rot
    			name : "Root",
				node : 0,
				// bone : 0
			},
			{
				// rot
				name : "Hip",
				node : 1,
				// bone : 29
			},
			{
				// rot
    			name : "RightUpperLeg",
				node : 5,
				// bone : 30
			},
			{
				// pos
    			name : "RightFootIKTarget",
				node : 8,
				//bone : 33
				bone : 38
			},
			{
    			name : "RightFoot",
				node : 9,
			},
			{
				name : "LeftUpperLeg",
				node : 12,
			},
			{
    			name : "LeftFootIKTarget",
				node : 15,
			},
			{
    			name : "LeftFoot",
				node : 16,
			},
			{
    			name : "Torso",
				node : 18,
			},
			{
    			name : "UpperTorsoIKTarget",
				node : 20,
			},
			{
    			name : "Unknown0x15",
				node : 21,
			},
			{
    			name : "HeadLookAtTarget",
				node : 23,
			},
			{
    			name : "RightShoulder",
				node : 25,
			},
			{
    			name : "RightArm",
				node : 26,
			},
			{
    			name : "RightHandIKTarget",
				node : 29,
			},
			{
    			name : "RightHand",
				node : 30,
			},
			{
				name : "LeftShoulder",
				node : 31,
			},
			{
    			name : "LeftArm",
				node : 32,
			},
			{
    			name : "LeftHandIKTarget",
				node : 33,
			},
			{
    			name : "LeftHandIKTarget_Dupe",
				node : 35,
			},
			{
    			name : "LeftHand",
				node : 36,
			}
		];

		// Animation

		for(let i = 0; i < nodes.length; i++) {

			if(i > 3) {
				break;
			}
			
			

			const node = nodes[i];
			const info = bone_lookup[i];
			const bone = bones[info.bone];

			if(node.id === 8) {
				console.log(node);
				let x = node.pos.x;
				let y = node.pos.y;
				let z = node.pos.z;
				node.pos = {
					x : x,
					y : y,
					z : z
				}
			}

			if(!bone) {
				continue;
			}
			
			if(node.pos) {
				
				let keys = new Array(this.mem.anim_length + 1);

				// First we need to merge all of the key frames

				for(let axis in node.pos) {

					node.pos[axis].forEach( f => {

						if(f.frame_index === 0 || f.frame_index === this.mem.anim_length) {
							if(f.val === undefined) {
								f.val = bone.position[axis];
							}
						}
						
						if(!keys[f.frame_index]) {
							keys[f.frame_index] = {
								frame : f.frame_index,
								x : null, 
								y : null,
								z : null
							};
						}
						
						if(axis === 'z') {
							keys[f.frame_index][axis] = f.val;
						} else {
							keys[f.frame_index][axis] = bone.position[axis];
						}

					});
					
					console.log(node.pos);
				}

				// Then we need to go back and solve to missing values

				for(let axis in node.pos) {
					
					keys.forEach( key => {
						if(!key) {
							return;
						}

						if(key[axis] !== undefined) {
							return;
						}
						
						let prev_val, next_val, prev_index, next_index;
						
						for(let k = key.frame; k >= 0; k--) {
							if(!keys[k]) {
								continue;
							}

							if(keys[k][axis] === undefined) {
								continue;
							}
							
							prev_val = keys[k][axis];
							prev_index = keys[k].frame;
							break;
						}

					});

				}

				keys = keys.filter( key => {
					return key;
				});
				
				let name = ".bones[" + bone.name + "].position";
				let times = [];
				let values = [];

				this.fillKeyValues(keys);
				keys.forEach(key => {
					
					times.push(key.frame / 30);
					values.push(key.x, key.y, key.z);

				});

				const track = new THREE.VectorKeyframeTrack(name, times, values);
				tracks.push(track);
				

			}

			// End Position

			// Start Rotation

			if(node.rot) {
				
				console.log(node);

				let array = bone.rotation.toArray();
				let og = {
					x : array[0],
					y : array[1],
					z : array[2]
				}
				let keys = new Array(this.mem.anim_length + 1);

				// First we need to merge all of the key frames

				for(let axis in node.rot) {

					node.rot[axis].forEach( f => {

						if(f.frame_index === 0 || f.frame_index === this.mem.anim_length) {
							if(f.val === undefined) {
								f.val = bone.position[axis];
							}
						}
						
						if(!keys[f.frame_index]) {
							keys[f.frame_index] = {
								frame : f.frame_index,
								x : null, 
								y : null,
								z : null
							};
						}
						
						if(node.id === 1) {
							if(axis !== "x") {
								keys[f.frame_index][axis] = f.val + Math.PI;
							} else {
								keys[f.frame_index][axis] = og[axis];
							}
						} else {
							keys[f.frame_index][axis] = f.val;
						}

					});

				}

				// Then we need to go back and solve to missing values

				for(let axis in node.rot) {
					
					keys.forEach( key => {
						if(!key) {
							return;
						}

						if(key[axis] !== undefined) {
							return;
						}
						
						let prev_val, next_val, prev_index, next_index;
						
						for(let k = key.frame; k >= 0; k--) {
							if(!keys[k]) {
								continue;
							}

							if(keys[k][axis] === undefined) {
								continue;
							}
							
							prev_val = keys[k][axis];
							prev_index = keys[k].frame;
							break;
						}

					});

				}

				keys = keys.filter( key => {
					return key;
				});
				
				let name = ".bones[" + bone.name + "].quaternion";
				let times = [];
				let values = [];

				this.fillKeyValues(keys);
				keys.forEach(key => {
					
					times.push(key.frame / 30);
					let e = new THREE.Euler(key.x, key.y, key.z);
					let q = new THREE.Quaternion();
					q.setFromEuler (e);
					values.push(q.x, q.y, q.z, q.w);

				});

				const track = new THREE.QuaternionKeyframeTrack(name, times, values);
				tracks.push(track);

			}

		}

		let clip = new THREE.AnimationClip(name, duration, tracks);

		return clip;

	}

	fillKeyValues(array) {

		const axis_keys = [ "x", "y", "z" ];
		
		axis_keys.forEach( axis => {
				
			while(this.nullCheck(array, axis)) {
				
				// First we find the null index
				
				let null_index;
				for(let i = 0; i < array.length; i++) {
					if(array[i][axis] || array[i][axis] === 0) {
						continue;
					}
					null_index = i;
					break;
				}
				
				// Which means the non-null index is one-before

				let first_index = null_index - 1;

				// Then we find the first following non-null index

				let non_null_index;
				for(let i = null_index; i < array.length; i++) {
					if(!array[i][axis] && array[i][axis] !== 0) {
						continue;
					}
					non_null_index = i;
					break;
				}
				
				// Then we find the diff between frames and between values
				
				let val_diff = array[non_null_index][axis] - array[first_index][axis];
				let frame_diff = array[non_null_index].frame - array[first_index].frame;

				let inc = val_diff / frame_diff;

				for(let i = null_index; i < non_null_index; i++) {
					frame_diff = array[i].frame - array[first_index].frame;
					array[i][axis] = array[first_index][axis] + inc * frame_diff;
				}

			}

		});

	}

	nullCheck(array, axis) {
		for(let i = 0; i < array.length; i++) {
			if(array[i][axis] !== null) {
				continue;
			}

			return true;
		}

		return false;
	}

	readKeyValues(node, type, axis) {

		let keyFrameCount
		
		if(this.mem.block_2_half) {
			keyFrameCount = this.view.getUint8(this.mem.block2Ofs);
			this.mem.block2Ofs++;
		} else {
			keyFrameCount = this.view.getUint16(this.mem.block2Ofs, true);
			this.mem.block2Ofs += 2;
		}

		node[type] = node[type] || {};
		node[type][axis] = [{
			frame_index : 0
		}];

		for(let i = 0; i < keyFrameCount; i++) {
			let keyFrame;
			
			if(this.mem.block_3_half) {
				keyFrame = this.view.getUint8(this.mem.block3Ofs);
				this.mem.block3Ofs++;
			} else {
				keyFrame = this.view.getUint16(this.mem.block3Ofs, true);
				this.mem.block3Ofs += 2;
			}

			node[type][axis].push({
				frame_index : keyFrame
			});
		}

		node[type][axis].push({
			frame_index : this.mem.anim_length
		});
		
		let index = 0;
		do {

			let byte = this.view.getUint8(this.mem.block4Ofs);
			this.mem.block4Ofs++

			if(byte === 0) {
				break;
			}
				
			// READ 1 of 4

			if(byte & 0xc0) {
					
				if(byte & 0x80) {
					let u0 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;
					let u1 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;

					node[type][axis][index].curve = [
						this.decodeFloat16(u0),
						this.decodeFloat16(u1)
					]
				};
					
				if(byte & 0x40) {
					let u0 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;

					node[type][axis][index].val = this.decodeFloat16(u0);
					if(type === 'rot') {
						node[type][axis][index].val *= 65535;
					}
				}

				index++;
			}
				
			// READ 2 of 4

			if(byte & 0x30) {
					
				if(byte & 0x20) {
					let u0 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;
					let u1 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;

					node[type][axis][index].curve = [
						this.decodeFloat16(u0),
						this.decodeFloat16(u1)
					]
				};
					
				if(byte & 0x10) {
					let u0 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;

					node[type][axis][index].val = this.decodeFloat16(u0);
					if(type === 'rot') {
						node[type][axis][index].val *= 65535;
					}
				}
				
				index++;
			}
				
			// READ 3 of 4

			if(byte & 0x0c) {
					
				if(byte & 0x08) {
					let u0 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;
					let u1 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;

					node[type][axis][index].curve = [
						this.decodeFloat16(u0),
						this.decodeFloat16(u1)
					]
				};
					
				if(byte & 0x04) {
					let u0 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;

					node[type][axis][index].val = this.decodeFloat16(u0);
					if(type === 'rot') {
						node[type][axis][index].val *= 65535;
					}
				}

				index++;
			}
				
			// READ 4 of 4

			if(byte & 0x03) {
					
				if(byte & 0x02) {
					let u0 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;
					let u1 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;

					node[type][axis][index].curve = [
						this.decodeFloat16(u0),
						this.decodeFloat16(u1)
					]
				};
					
				if(byte & 0x01) {
					let u0 = this.view.getUint16(this.mem.block5Ofs, true);
					this.mem.block5Ofs += 2;

					node[type][axis][index].val = this.decodeFloat16(u0);
					if(type === 'rot') {
						node[type][axis][index].val *= 65535;
					}
				}
				
				index++;
			}
		

		} while(index < node[type][axis].length);
		

	}

	calc_keyframe_block_size(value) {
		let result = 0
		result += (value & 0x80) ? 2 : 0
		result += (value & 0x40) ? 1 : 0
		result += (value & 0x20) ? 2 : 0
		result += (value & 0x10) ? 1 : 0
		result += (value & 0x08) ? 2 : 0
		result += (value & 0x04) ? 1 : 0
		result += (value & 0x02) ? 2 : 0
		result += (value & 0x01) ? 1 : 0
		return result
	}

	decodeFloat16 (binary) {

		var exponent = (binary & 0x7C00) >> 10;
		var fraction = binary & 0x03FF;

		return (binary >> 15 ? -1 : 1) * (
			exponent ?
			(
				exponent === 0x1F ?
				fraction ? NaN : Infinity :
				Math.pow(2, exponent - 15) * (1 + fraction / 0x400)
			) : 6.103515625e-5 * (fraction / 0x400)
		);
	
	}

}

