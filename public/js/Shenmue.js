			
import * as THREE from '../three.module.js';
import Stats from '../mod/libs/stats.module.js';
import { GUI } from '../mod/libs/dat.gui.module.js';

import { OrbitControls } from '../mod/controls/OrbitControls.js';
import { OutlineEffect } from '../mod/effects/OutlineEffect.js';
import { MMDLoader } from '../mod/loaders/MMDLoader.js';
import { MT5Loader } from '../mod/loaders/MT5Loader.js';
import { MMDAnimationHelper } from '../mod/animation/MMDAnimationHelper.js';

var container, stats;
var mesh, camera, scene, renderer, effect;
var helper, ikHelper, physicsHelper;
var clock = new THREE.Clock();

init();
initGui();
animate();

function init() {

	console.log("is this being updated?");


	container = document.createElement( 'div' );
	document.body.appendChild( container );

	// CAMERA

	camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 2000 );
	camera.position.z = 5;
	camera.position.y = 2;

	// SCENE

	scene = new THREE.Scene();
	scene.background = new THREE.Color( 0xffffff );

	var gridHelper = new THREE.PolarGridHelper( 30, 10 );
	gridHelper.position.y = -1.1;
	scene.add( gridHelper );

	var ambient = new THREE.AmbientLight( 0x666666 );
	scene.add( ambient );

	var directionalLight = new THREE.DirectionalLight( 0x887766 );
	directionalLight.position.set( - 1, 1, 1 ).normalize();
	scene.add( directionalLight );

	// RENDER
	
	renderer = new THREE.WebGLRenderer( { antialias: true } );
	renderer.setPixelRatio( window.devicePixelRatio );
	renderer.setSize( window.innerWidth, window.innerHeight );
	container.appendChild( renderer.domElement );
	effect = new OutlineEffect( renderer );

	// STATS

	stats = new Stats();
	container.appendChild( stats.dom );

	// model


	var modelFile = 'assets/ryo.mt5';
	var motnFile = 'assets/A_WALK_L_02.BIN';

	helper = new MMDAnimationHelper( {
		afterglow: 1.0
	});

	var loader = new MT5Loader();
	
	loader.loadWithAnimation( modelFile, motnFile, function ( mesh, anim ) {

		scene.add( mesh );
		window.mesh = mesh;
		mesh.position.y = -1.1;
		
		let skelHelper = new THREE.SkeletonHelper(mesh);
		skelHelper.material.lineWidth = 5;
		scene.add(skelHelper);

		helper.add( mesh, {
			animation: anim,
			physics : false
		});
		
		ikHelper = helper.objects.get( mesh ).ikSolver.createHelper();
		ikHelper.visible = true;
		scene.add( ikHelper );
		
		ikHelper.children.forEach( child => {
			if(child.type !== "Mesh") {
				return;
			}
		
			child.scale.x = 0.14;
			child.scale.y = 0.14;
			child.scale.z = 0.14;
		});

	});

	var controls = new OrbitControls( camera, renderer.domElement );
	controls.minDistance = 0.5;
	controls.maxDistance = 100;

	window.addEventListener( 'resize', onWindowResize, false );

	var phongMaterials;
	var originalMaterials;

	function makePhongMaterials( materials ) {

		var array = [];

		for ( var i = 0, il = materials.length; i < il; i ++ ) {

			var m = new THREE.MeshPhongMaterial();
			m.copy( materials[ i ] );
			m.needsUpdate = true;

			array.push( m );

		}

		phongMaterials = array;

	}

}

function initGui() {

	var api = {
		'animation': true,
		'ik': true,
		'outline': true,
		'show IK bones': true,
	};

	var gui = new GUI();
	gui.add( api, 'animation' ).onChange( function () {
		helper.enable( 'animation', api[ 'animation' ] );
	});
	
	gui.add( api, 'ik' ).onChange( function () {
		helper.enable( 'ik', api[ 'ik' ] );
	});

	gui.add( api, 'outline' ).onChange( function () {
		effect.enabled = api[ 'outline' ];
	});
	
	gui.add( api, 'show IK bones' ).onChange( function () {
		ikHelper.visible = api[ 'show IK bones' ];
	});

}

// Window Resize

function onWindowResize() {

	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();

	effect.setSize( window.innerWidth, window.innerHeight );

}

// Animate

function animate() {

	requestAnimationFrame( animate );

	stats.begin();
	render();
	stats.end();

}

// Render

function render() {

	helper.update( clock.getDelta() );
	effect.render( scene, camera );

}

