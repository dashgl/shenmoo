"use strict";

const Viewport = (function() {

	this.MEM = {
		clock : new THREE.Clock()
	}

	this.DOM = {
		section : document.getElementById('section')
	}

	this.EVT = {
		handleWindowResize : evt_handleWindowResize.bind(this)
	}

	this.API = {
		loadRyo : api_loadRyo.bind(this),
		animate : api_animate.bind(this)
	}

	init.apply(this);
	return this;

	function init() {
		
		this.MEM.scene = new THREE.Scene();
		this.MEM.scene.background = new THREE.Color(0xcccccc);

		this.MEM.renderer = new THREE.WebGLRenderer({
			antialias: true
		});

		let width = this.DOM.section.offsetWidth;
		let height = this.DOM.section.offsetHeight;
		this.MEM.renderer.setPixelRatio(window.devicePixelRatio);
		this.MEM.renderer.setSize(width, height);
		this.DOM.section.appendChild(this.MEM.renderer.domElement);

		this.MEM.camera = new THREE.PerspectiveCamera(60, width / height, 0.01, 1000);
		this.MEM.camera.position.set(0, 0, -5);
		this.MEM.camera.lookAt(new THREE.Vector3(0,0,0));

		this.MEM.controls = new THREE.OrbitControls(this.MEM.camera, this.DOM.section);
		this.MEM.controls.enableDamping = true;
		this.MEM.controls.dampingFactor = 0.25;
		this.MEM.controls.screenSpacePanning = false;
		// this.MEM.controls.maxPolarAngle = Math.PI / 2;
		
		let grid = new THREE.GridHelper(100, 10);
		// grid.position.y = -1.1;
		this.MEM.scene.add(grid);
		this.MEM.scene.add(new THREE.AmbientLight(0xffffff));

		window.addEventListener('resize', this.EVT.handleWindowResize);
		
		this.MEM.db = new Dexie("shenmue_test");
		this.MEM.db.version(1).stores({
			matrix : '&[Id+Frame]'
		});


		this.API.loadRyo();
		this.API.animate();

	}

	function evt_handleWindowResize() {

		console.log("window resize");

	}

	function api_animate() {

		window.requestAnimationFrame(this.API.animate);


		let delta = this.MEM.clock.getDelta();
		if(this.MEM.mixer) {
			this.MEM.mixer.update(delta);
		}

		this.MEM.controls.update();
		this.MEM.renderer.render(this.MEM.scene, this.MEM.camera);


	}

	async function api_loadRyo() {

		// Load Skeleton

		let response = await fetch('assets/ryo.mt5');
		let buffer = await response.arrayBuffer();

		let loader = new THREE.MT5Loader(buffer);
		let mesh = loader.parse();
		let skelHelper = new THREE.SkeletonHelper(mesh);
		skelHelper.material.linewidth = 3;

		// MMD IK Helper
		
		console.log(THREE.MMDLoader)

		/*
		let mmdHelper = new THREE.MMDAnimationHelper();
		let ikHelper = mmdHelper.objects.get( mesh ).ikSolver.createHelper();a
		this.MEM.scene.add(ikHelper);
		ikHelper.visible = false;
		*/

		// Load motion

		response = await fetch('assets/A_WALK_L_02.BIN');
		buffer = await response.arrayBuffer();
		loader = new THREE.MotionLoader(mesh, buffer);
		let clip = loader.parse(mesh.skeleton.bones);

		// const clip = this.API.loadMatrix(list, mesh);
		
		if(clip) {
			this.MEM.mixer = new THREE.AnimationMixer(mesh);
			let action = this.MEM.mixer.clipAction(clip, mesh);
			action.timeScale = 0.3;
			action.play();
		}

		// Add to scene

		this.MEM.scene.add(mesh);
		this.MEM.scene.add(skelHelper);

	}


}).apply({});
