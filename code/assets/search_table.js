"use strict";

const fs = require('fs');
const buffer = fs.readFileSync('motion.bin');

const header = {
	sequence_table_ofs : buffer.readUInt32LE(0x00),
	sequence_names_ofs : buffer.readUInt32LE(0x04),
	sequence_data_ofs : buffer.readUInt32LE(0x08),
	sequence_count : buffer.readUInt32LE(0x0c),
	filelength : buffer.readUInt32LE(0x10),
}

let min = null;
const WALK = 716276;
const SEQ_LIST = new Array(300);

let ofs_a = header.sequence_table_ofs;
let ofs_b = header.sequence_names_ofs;

for(let i = 0; i < SEQ_LIST.length; i++) {

	SEQ_LIST[i] = {
		dataOfs : buffer.readUInt32LE(ofs_a + 0x00),
		ptr_b : buffer.readUInt32LE(ofs_a + 0x04),
		name : ""
	}
	ofs_a += 8;

	let name_ptr = buffer.readUInt32LE(ofs_b);
	ofs_b += 4;

	let ch;
	while( (ch = buffer.readUInt8(name_ptr++)) !== 0) {
		SEQ_LIST[i].name += String.fromCharCode(ch);
	}

	SEQ_LIST[i].dataOfs += header.sequence_data_ofs;

	if(SEQ_LIST[i].dataOfs === WALK) {
		console.log("Walk found at: %d", i);
		console.log(SEQ_LIST[i]);
		continue;
	}

	if(SEQ_LIST[i].dataOfs < WALK) {
		continue;
	}

	if(!min) {
		min = SEQ_LIST[i];
		continue;
	}

	if(min.dataOfs < SEQ_LIST[i].dataOfs) {
		continue;
	}

	min = SEQ_LIST[i];

}


console.log(header);
console.log(SEQ_LIST[SEQ_LIST.length - 1]);

console.log("----- min ------");
console.log(min);

let bin = buffer.subarray(WALK, min.dataOfs);
fs.writeFileSync("AKI_AKI_RUN_START.BIN", bin);

